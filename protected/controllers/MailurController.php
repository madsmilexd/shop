<?php

class MailurController extends Controller
{
	//return to main page(get)
	public function actionIndex()
	{
		$this->redirect(array('/site'));
	}

	//approve email (set)
	public function actionAprove($url = null)
	{
		Meta::make($this, 'Подтвердить аккаунт');


		/** @var User $usr */
		$usr = User::model();
		if ($url == null) {
			$this->render('aprove', array('model' => 'Something went wrong try again or contact with administrator ' .
				'<a href="mailto:' . Yii::app()->params['adminEmail'] . '">' . Yii::app()->params['adminEmail'] . '</a>'));
		} else {
			if ($usr->aproveMe($url))
				$this->redirect(array('/site'));
		}
	}

	//pass recovery (set)
	public function actionCheck($url)
	{
		Meta::make($this, 'Проверить аккаунт');

		/** @var User $usr */
		$usr = User::model();
		$usr->changePass(true, $url);

		$this->redirect(array('/site'));
	}

    public function actionNew($url = null){
		Meta::make($this, 'Подтвердить аккаунт');

		/** @var Admin $model */
        $model = new Admin('ChangePass');
        if(isset($url))
        {
            if(Yii::app()->request->getPost('Admin'))
            {
                if(Yii::app()->request->getPost('Admin')[pass] != Yii::app()->request->getPost('Admin')[pass2])
                {$model->addError('pass', 'Passwords doesnt match');
                    $this->render('new',array('model'=>$model));
                }
                $model->changePass(true,$url);
                $this->redirect(array('/site'));
            }
            else
            {
                $this->render('new',array('model'=>$model));
            }
        }
    }

	//password recovery(get)
	public function actionRecovery()
	{
		Meta::make($this, 'Восстановить аккаунт');


		if (!Yii::app()->user->isGuest) {
			$this->redirect(Yii::app()->user->returnUrl);
		}

		$model = new Admin('passRecovery');
		/** @var User $model */
		if (isset($_POST['Admin']['email'])) {
			$model = Admin::model()->findByMail($_POST['Admin']['email'])->find();
			if (!$model) {
				$model = new Admin();
				$model->addError('email', 'Email address is incorrect');
				$this->render('recover', array('model' => $model));

			} else {
				if ($model->passRecovery()) {
					$this->redirect(array('/site'));
				} else {
					$this->render('recover', array('model' => $model));
				}
			}
		} else {
			$this->render('recover', array('model' => $model));
		}
	}

}