<?php

class ClientController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','create','update','index','view','search'),
				'roles'=>array(Admin::STATUS_MANAGER),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete'),
				'roles'=>array(Admin::STATUS_ADMIN),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		Meta::make($this, 'Просмотреть данные клиента');

		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{

		Meta::make($this, 'Создать клиента');

		$model=new Client;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Client']))
		{
			$model->attributes=$_POST['Client'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

    public function actionSearch(){
		Meta::make($this, 'Новый клиента');

		$client = new Client('search');
        if(Yii::app()->request->getPost('Client'))
        {
            $client->attributes = Yii::app()->request->getPost('Client');
            if(Client::model()->findByAttributes(array('email'=>$client->email)))
            {
                $model = Client::model()->findByAttributes(array('email'=>$client->email));
                $this->redirect(Yii::app()->createAbsoluteUrl('/client/'.$model->id));
            }
            else{
            if(Client::model()->findByAttributes(array('email2'=>$client->email)))
            {
                $model = Client::model()->findByAttributes(array('email2'=>$client->email));
                $this->redirect(Yii::app()->createAbsoluteUrl('/client/'.$model->id));
            }
                else{
                    $answer = 'No such Email in the data base';
                    $this->render('search',array('model'=>$client,'answer'=>$answer));}
            }
        }
        else
        {
            $answer = '';
            $this->render('search',array('model'=>$client,'answer'=>$answer));
        }

    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		Meta::make($this, 'Редактировать клиента');

		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Client']))
		{
			$model->attributes=$_POST['Client'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		Meta::make($this, 'Удалить клиента');

		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionActivate($id)
	{
		Meta::make($this, 'Активировать клиента');

		$this->loadModel($id)->activate();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
	$this->redirect(Yii::app()->createAbsoluteUrl('client/admin'));
	}

    public function actionSendPassword($id){
		Meta::make($this, 'Отправить пароль клиенту');

		$model = Client::model()->findByPk($id);
        $model->sendMail('Your password is '.$model->password,$model->email);
        $model->sendMail('Your password is '.$model->password,$model->email2);
        $this->redirect(Yii::app()->createAbsoluteUrl('client'));
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		Meta::make($this, 'Просмотр списка клиентов');

		$model=new Client('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Client']))
			$model->attributes=$_GET['Client'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Client the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Client::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Client $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='client-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
