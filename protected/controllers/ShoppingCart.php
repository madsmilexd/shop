<?php

/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 18.09.14
 * Time: 18:42
 */
class ShoppingCart
{
    public function getItems()
    {
        $res = array();
        $cart = Yii::app()->session['cart'];
        for ($i = 0; $i < count($cart); $i++) {
            $res[$i] = unserialize($cart[$i]);
        }
        return $res;
    }

    public function init()
    {
        //echo count(Yii::app()->session['cart']);
    }

    public function put($id)
    {
        $temp = array();
        $goods = Goods::model()->findByPk($id);
        $in_cart = false;
        for ($q = 0; $q < count(Yii::app()->session['cart']); $q++) {
            $temp[$q] = Yii::app()->session['cart'][$q];
            if (!$in_cart) {
                $item = unserialize(Yii::app()->session['cart'][$q]);
                if ($item->id == $goods->id) {
                    $in_cart = true;
                }
            }
        }

        if (!$in_cart) {
            $temp[count($temp)] = serialize($goods);
        }

        Yii::app()->session['cart'] = $temp;

    }

    public function remove($id)
    {
        $temp = array();
        $i = 0;

        foreach (Yii::app()->session['cart'] as $item) {
            $uns = unserialize($item);
            if ($uns->id != $id) {
                $temp[$i++] = $item;
            }

            Yii::app()->session['cart'] = $temp;
        }


        return true;
    }

    public function clear()
    {
        unset(Yii::app()->session['cart']);
    }

    public function makeCartList()
    {
        $res = '';
        if (Yii::app()->session['cart']) {
            foreach (Yii::app()->session['cart'] as $item) {
                $res .= self::makeUrl($item);
            }
            $res .= '<a href="' . Yii::app()->createAbsoluteUrl('site/checkout/') . '">
                <div class="list-group-item	list-group-item-success">
                    Оформить заказ
                </div>
        	</a>';
            $res .= '<a href="' . Yii::app()->createAbsoluteUrl('site/clear/') . '">
                <div class="list-group-item	list-group-item-danger">
                Очистить корзину
                </div>
            </a>';
        } else {
            $res .= '<a href="' . Yii::app()->createAbsoluteUrl('site/index/') . '"class="list-group-item last-item
	list-group-item-info">Открыть
	каталог
	</a>';
        }
        return $res;
        //var_dump(Yii::app()->session['cart']);
    }

    public function makeUrl($elem)
    {
        /** @var Goods $item */
        $item = unserialize($elem);
        $res = '
<div class="btn-group">
		        <div class="col-xs-12" style="border-top:none;border-bottom:none;border-left:1px solid black; border-right:1px solid black;">

             <a class="btn btn-default col-xs-10" href="' . Yii::app()->createAbsoluteUrl('site/view/' . $item->id) .'">
            ' . $item->name . '(' . $item->getPriceLabel() . ' руб.)
             </a>
              <a class="btn btn-danger col-xs-2" href="' . Yii::app()->createAbsoluteUrl('site/unsete/' .
                $item->id) .
            '">
                  <span class="glyphicon glyphicon-remove-circle"></span> &nbsp;
              </a>
</div>
        </div>
        ';

        return $res;
    }

    public function goodsToArray($goods)
    {
        return array('id' => $goods->id, 'price' => $goods->pricex100, 'name' => $goods->name);
    }
}
