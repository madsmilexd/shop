<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	//просмотр всех товаров $opp количество товаров на странице, в идеале кратное 3
	public function actionIndex($opp = 12)
	{
		Meta::make($this);

		$this->render('search');
//		$criteria = Goods::model()->with('category')->getDbCriteria();
//		$pages = new CPagination(Goods::model()->count($criteria));
//		$pages->pageSize = $opp;
//		$pages->applyLimit($criteria);
//		$goods = Goods::model()->findAll($criteria);
//		$this->render('index', array('goods' => $goods, 'pages' => $pages));
	}

    public function actionCatalog($opp = 12)
    {

		Meta::make($this, 'Каталог товаров');

		$criteria = Goods::model()->with('category')->getDbCriteria();
		$pages = new CPagination(Goods::model()->count($criteria));
		$pages->pageSize = $opp;
		$pages->applyLimit($criteria);
		$goods = Goods::model()->findAll($criteria);
		$this->render('index', array('goods' => $goods, 'pages' => $pages));
    }

	//просмотр товара
	public function actionView($id)
	{
		//
		$item = Goods::model()->with('category')->findByPk($id);
		if ($item) {
			Meta::make($this, $item->name, $item->kw, $item->ds);
			$this->render('view', array('item' => $item));
		} else {
			Meta::make($this, 'Просмотреть товар');
			$this->render('view', array('error' => 'Not Found'));

		}
	}

	//добавить элемент в корзину
	public function actionPut($id)
	{
		Yii::app()->ShoppingCart->put($id);
		Yii::app()->user->setFlash('success','Товар успешно добавлен в корзину');
		$this->redirect(Yii::app()->createAbsoluteUrl('site/catalog'));
	}

	//удалить элемент из корзины
	public function actionUnsete($id)
	{
		Yii::app()->ShoppingCart->remove($id);
		Yii::app()->user->setFlash('success','Товар успешно удален из корзины');

		$this->redirect(Yii::app()->createAbsoluteUrl('site/catalog'));
	}

	//очистить корзину
	public function actionClear($status = 'nothing')
	{
		Yii::app()->ShoppingCart->clear();
		//die($status);
		if($status=='kop'){
			Yii::app()->user->setFlash('info','Заказ оформлен');
		}
		Yii::app()->user->setFlash('success','Корзина очищена');
		$this->redirect(Yii::app()->createAbsoluteUrl('site/catalog'));


	}

	//сделать заказ
	public function actionCheckout()
	{
		Meta::make($this, 'Оформить заказ');

		$order = new Orders();
		if(Yii::app()->request->getPost('Orders')){
			$order->attributes = Yii::app()->request->getPost('Orders');
			$order->payment = 'Наличные';
			if($order->save()){
				$this->redirect(Yii::app()->createAbsoluteUrl('site/clear?status=kop'));
			}
		}
		$this->render('checkout',array('model'=>$order));
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	//сообщение об ошибке
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (!Yii::app()->request->isAjaxRequest)
				$this->render('error', $error);
		}
	}

	//Логин
	public function actionLogin()
	{
		Meta::make($this, 'Авторизация');

		if (!Yii::app()->user->isGuest) {
			$this->redirect(Yii::app()->user->returnUrl);
		}
		$model = new Admin('login');

		if ($model->attributes = Yii::app()->request->getPost('Admin')) {

			if ($model->login()) {
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		$this->render('login', array('model' => $model));
	}


	//вывод списка категорий
	public function actionCategories()
	{
		$categories = Category::model()->findAll();
		$this->render('category', array('items' => $categories));
	}

	//вывод по категориям
	public function actionBy($id)
	{
		/** @var Category $cat */
		$cat = Category::model()->findByPk($id);
		Meta::make($this, 'Все товары из'.$cat->name);

		$criteria = Goods::model()->with('category')->byCategory($id)->getDbCriteria();
		$pages = new CPagination(Goods::model()->count($criteria));
		$pages->pageSize = 12;
		$pages->applyLimit($criteria);
		$goods = Goods::model()->findAll($criteria);
		$this->render('index', array('goods' => $goods, 'pages' => $pages));
	}

	//обратная связь
	public function actionBack()
	{
		if (Yii::app()->request->getPost('Mail')) {
			$from = Yii::app()->request->getPost('Mail');

			$message = "Message sent!";
			$mail = Yii::app()->mailer;
			$mail->SetFrom(Yii::app()->params['techemail'], Yii::app()->params['project']);
			$mail->AddAddress(Yii::app()->params['shopEmail'], Yii::app()->params['adminName']);
			$mail->IsHTML(true); // set email format to HTML
			$mail->Subject = "С сайта " . Yii::app()->params['project'];
			$mail->Body = 'Сообщение от ' . $from['name'] . '(' . $from['email'] . ')<br/>' .
				(($from['tel']) ? 'Тел: ' . $from['tel'] . '<br/>' : '') .
				$from['message'];
			$mail->AltBody = 'Сообщение от ' . $from['name'] . '(' . $from['email'] . ')\n' .
				(($from['tel']) ? 'Тел: ' . $from['tel'] . '\n' : '') .
				$from['message'];
			if (!$mail->Send()) {
				$this->render('back', array('status' => 'fail'));
			} else {
				$this->render('back', array('status' => 'success'));
			}
		}
		$this->render('back');
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	//выход
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}


	public function actionSearch($kw, $limit = 20, $offset = 0,$new = -1,$page = 0)
	{

		$this->renderPartial('search',array('kw'=>$kw));
		if(strlen($kw)<=3){
			return false;
		}
		$goods = Goods::model();
		$cat = Category::model()->findByAttributes(array('name' => $kw));
		$wh = Warehouse::model()->findByAttributes(array('description' => $kw));
		$criteria = new CDbCriteria;
		$criteria->compare('id', $kw, 'OR');
		$criteria->compare('code', $kw, true, 'OR');
		$criteria->compare('category_id', $kw, true, 'OR');
		$criteria->compare('category_id', $kw, true, 'OR');
		$criteria->compare('name', $kw, true, 'OR');
		if ($cat)
			$criteria->compare('path', $cat->id, true, 'OR');

		$criteria->compare('description', $kw, true, 'OR');
		$criteria->compare('pricex100', $kw, true, 'OR');
		if ($wh)
			$criteria->compare('warehouse_id', $wh->id, true, 'OR');

		$criteria->compare('warehouse_id', $kw, true, 'OR');

		if($new == -1){
		$count = Goods::model()->count($criteria);
		}else{
			$count = $new;
		}
		$criteria->limit = $limit;
		$criteria->offset = $page*$limit;
		$res = Goods::model()->findAll($criteria);

		echo '<div class="btn-group">';
		for($i=0;$i<intval($count/$limit);$i++){
			$active = '';
			if($i == $page){
				$active = 'active';
			}
			echo ((intval($count/$limit))>1)?"<span class='btn btn-success spager $active' data-offset='$offset'
			data-kw='$kw'
			data-count='$count' data-limi='$limit'
		 data-page='$i'>".($i+1)."</span>":($count/$limit);
		}
		echo '</div><hr/>';

		foreach ($res as $item) {
			echo $this->makeDiv($item);
		}

	}

	protected function makeDiv($item)
	{
		$div = '';
		$div .= '<div class="col-sm-3 goods_item">';

		$div .= '<h2>#' . $item->id . ' ' . $item->name . '</h2>';
		$div .= '<sup>' . $item->code . '</sup><br/>';
		$div .= $item->getImage();
		$div .= '<p>';
		if (strlen($item->description) >= 200) {
			$div .= substr($item->description, 0, 200) . '...';
		} else {
			$div .= $item->description;
		}
		$div .= '</p>';
		$div .= '<p>' . $item->getPriceLabel() . '</p>';
		$div .= '<p>' . $item->getStatus() . '</p>';
		$div .= '<p><a class="btn btn-default" href="' . Yii::app()->createAbsoluteUrl('site/view/' . $item->id) . '"
	  role="button">Подробней»</a>';

		if ($item->status != 0)
			$div .= '<a class="btn btn-success" href="' . Yii::app()->createAbsoluteUrl('site/put/' . $item->id) . '"
		   role="button">Заказать</a>';
		$div .= '</p>
</div>';
		return $div;
	}
}