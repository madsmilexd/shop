<?php
/* @var $this WarehouseController */
/* @var $model Warehouse */

$this->breadcrumbs=array(
	'Warehouses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Создать склад', 'url'=>array('create')),
	array('label'=>'Редактировать склад', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить склад', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы действительно хотите удалить?')),
	array('label'=>'Управение складами', 'url'=>array('admin')),
);
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Просмотр склада №<?php echo $model->id; ?></h1>
    </div>
    <div class="col-xs-12 block_inner">

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'number',
		'description',
	),
)); ?>
        </div></div>
