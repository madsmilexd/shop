<?php
/* @var $this WarehouseController */
/* @var $model Warehouse */

$this->breadcrumbs=array(
	'Warehouses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Управление скаладами', 'url'=>array('admin')),
);
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Создание склада</h1>
    </div>
    <div class="col-xs-12 block_inner">

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
        </div></div>