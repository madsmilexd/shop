<?php
/* @var $this WarehouseController */
/* @var $model Warehouse */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'warehouse-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'number', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'number', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'number',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'description', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'description', array('rows'=>6, 'cols'=>50,'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'description',array('class'=>"label label-danger col-xs-10")); ?>
    </div>

	<div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',array('class'=>'btn btn-success')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->