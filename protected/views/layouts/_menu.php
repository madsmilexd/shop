<?php

function makeStatusDiv(){
	$res = '';

	if(Yii::app()->user->role_id){

		if(Yii::app()->user->role_id<=1){
			$res = '<span class="label-warning label text-left pull-left" style="border-radius: 0 0 3px 3px;padding-top: 15px">Manager</span>';
		}

		if(Yii::app()->user->role_id==2){
			$res = '<span class="label-danger label text-left pull-left" style="border-radius: 0 0 3px 3px;padding-top: 15px">Admin</span>';
		}
	}
	echo $res;
}

function getItems()
{
	$user = Yii::app()->user;
	return array(
		array('label'=>Yii::app()->params['project'], 'url'=>Yii::app()->createAbsoluteUrl('/'),
			'visible'=>$user->isGuest),
		array('label' => 'Главная', 'url' => Yii::app()->createAbsoluteUrl('/site/index')),
		array('label' => 'Каталог', 'url' => Yii::app()->createAbsoluteUrl('/site/catalog')),
//		array('label' => 'Категории', 'url' => Yii::app()->createAbsoluteUrl('/site/categories')),
		array('label' => 'Обратная связь', 'url' => Yii::app()->createAbsoluteUrl('/site/back'),
			'visible' => $user->isGuest),
		array('label'=>'Отследить заказ','url'=>array('/orders/check')),
		array('label' => 'Выход ||' ,'url' => array('/site/logout'), 'visible' => !$user->isGuest),
		array('label' => 'Вход', 'url' => array('/site/login'), 'visible' => $user->isGuest),
		array('label'=>'Товары','url'=>array('/goods/index'),'visible'=> $user->checkAccess(Admin::STATUS_MANAGER)),

		array('label'=>'Добавить товар', 'url'=>array('create'),'visible'=>(Yii::app()
					->controller->id=="goods"&&Yii::app()
					->controller->action->id=='index')?true:false),
		array('label'=>'Быстрый просмотр товаров', 'url'=>array('admin'),'visible'=>(Yii::app()
					->controller->id=="goods"&&Yii::app()->controller->action->id=='index')?true:false),

		array('label'=>'Быстрый просмотр товаров', 'url'=>array('admin'),'visible'=>(Yii::app()
					->controller->id=="goods"&&Yii::app()->controller->action->id=='create')?true:false),

		array('label'=>'Добавить товар', 'url'=>array('create'),'visible'=>(Yii::app()
					->controller->id=="goods"&&Yii::app()
					->controller->action->id=='admin')?true:false),

		array('label'=>'Добавить товар', 'url'=>array('create'),'visible'=>(Yii::app()
					->controller->id=="goods"&&Yii::app()
					->controller->action->id=='update')?true:false),
		array('label'=>'Быстрый просмотр товаров', 'url'=>array('admin'),'visible'=>(Yii::app()
					->controller->id=="goods"&&Yii::app()->controller->action->id=='update')?true:false),



		array('label'=>'Клиенты','url'=>array('/client/index'),'visible'=> $user->checkAccess(Admin::STATUS_MANAGER)),


//		array('label'=>'Города','url'=>array('/city/index'),'visible'=> $user->checkAccess
//				(Admin::STATUS_MANAGER)),



		array('label'=>'Склады','url'=>array('/warehouse/index'),'visible'=> $user->checkAccess
				(Admin::STATUS_MANAGER)),




//		array('label'=>'Автомобили','url'=>array('/cars/index'),'visible'=> $user->checkAccess(Admin::STATUS_MANAGER)),
		array('label'=>'Категории товаров','url'=>array('/category/index'),'visible'=> $user->checkAccess
				(Admin::STATUS_MANAGER)),

		array('label'=>'Сотрудники','url'=>array('/admin/index'),'visible'=> $user->checkAccess(Admin::STATUS_ADMIN)),
        array('label'=>'Списки заказов', 'url'=>array('orders/admin'),'visible'=>$user->checkAccess(Admin::STATUS_MANAGER)),
        array('label'=>'Проверка email', 'url'=>array('client/search'),'visible'=>$user->checkAccess(Admin::STATUS_MANAGER)),
	);
}


?>





<div class="navbar navbar-inverse" style=" margin-bottom:0px !important;border-radius: 0px;" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<?
			makeStatusDiv();
			$this->widget('zii.widgets.CMenu', array(
				'items' => getItems(),
				'htmlOptions' => array('class' => 'nav navbar-nav')
			));
			?>
		</div>
	</div>
</div>




