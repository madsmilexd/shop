<?php /* @var Controller $this */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="ru"/>

    <?php
    $this->cs->registerCssFile(Yii::app()->request->baseUrl . '/vendors/bootstrap/css/bootstrap.min.css');
    $this->cs->registerCssFile(Yii::app()->request->baseUrl . '/vendors/bootstrap/css/bootstrap-theme.css');
	$this->cs->registerCssFile(Yii::app()->request->baseUrl . '/css/style.css');
	$this->cs->registerCssFile('http://static.jstree.com/3.0.4/assets/dist/themes/default/style.min.css');
	$this->cs->registerScriptFile(Yii::app()->request->baseUrl . '/vendors/jquery/jquery-2.1.0.min.js', CClientScript::POS_END);
	$this->cs->registerScriptFile(Yii::app()->request->baseUrl . '/vendors/bootstrap/js/bootstrap.min.js', CClientScript::POS_END);
	$this->cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/growl.js', CClientScript::POS_END);
	$this->cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/jstree.js', CClientScript::POS_END);
	$this->cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/app.js', CClientScript::POS_END);
    ?>


	<title><?=isset($this->pageTitle) ? $this->pageTitle : Yii::app()->params['project']; ?></title>

</head>

<body>

	<? $fl = Yii::app()->user->getFlashes(true);
	foreach($fl as $k=>$v){
		echo '<div class="notif hidden">'.$k.';'.$v.'</div>';
	}
	?>

<div class="col-xs-12" style="padding: 0px !important;">
    <? include_once('_menu.php'); ?>

    <div class="col-xs-12" style="background-color: #aaaaab;" align="center">

        <div class="col-lg-offset-1 col-lg-10 middle">
			<div align="center">

				<input name="Search[sw]" placeholder="Поиск" class="form-control" style="height: 32px;" type="search"
					   id="search" data-searchurl="<?=Yii::app()
					->createAbsoluteUrl('site/search');?>"
					   data-here="<?=Yii::app()->createAbsoluteUrl(Yii::app()->controller->id.'/'.Yii::app()
							   ->controller->action->id);?>" value="<?=($kw)?$kw:'';?>" autofocus>

			</div>
            <div class="<?=(Yii::app()->controller->id=='site')?'col-xs-9':'col-xs-12'?>">
                <?= $content; ?>
            </div>
            <? include_once('_rbar.php'); ?>
        </div>

    </div>




    <div class="footer" align="center">
        <div class="signature">

			By <a href="http://vk.com/madsmilexd">MadSmileXD</a> &
			<a href="https://vk.com/id13188219">Unlite</a> 2014
        </div>
    </div>




</div>
</body>
</html>
