<?php
/* @var $this AdminController */
/* @var $model Admin */

$this->breadcrumbs=array(
	'Admins'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Создать сотрудника', 'url'=>array('create')),
	array('label'=>'Просмотр сотрудника', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление сотрудниками', 'url'=>array('admin')),
);
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Редактирование сотрудника №<?php echo $model->id; ?></h1>
    </div>
    <div class="col-xs-12 block_inner">
<?php $this->renderPartial('_form', array('model'=>$model)); ?></div></div>