<?php
/* @var $this AdminController */
/* @var $model Admin */

$this->breadcrumbs=array(
	'Admins'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Создать сотрудника', 'url'=>array('create')),
	array('label'=>'Изменение сотрудника', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить сотрудника', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы действительно хотите удалить?')),
	array('label'=>'Управление сотрудниками', 'url'=>array('admin')),
);
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Сотрудник №<?php echo $model->id; ?></h1>
    </div>
    <div class="col-xs-12 block_inner">

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'role_id',
		'login',
		'email',
		'description',
	),
)); ?>
</div></div>