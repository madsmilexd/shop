<?php
/* @var $this AdminController */
/* @var $model Admin */

$this->breadcrumbs=array(
	'Admins'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Создать сотрудника', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#admin-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Управление сотрудниками</h1>
    </div>
    <div class="col-xs-12 block_inner">
<?php echo CHtml::link('Продвинутый поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'admin-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'summaryText'=>false,
//	'filter'=>$model,
	'columns'=>array(
		'id',
		'role_id',
		'login',
		'email',
		'description',
        array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => '{update}{view}{pass}{delete}',
            'htmlOptions' => array('class'=>'btn-group col-xs-3'),
            'buttons' => array
            (
                'update' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class="glyphicon glyphicon-pencil"></span>',
                    'options' => array('title' => 'Редактировать', 'class' => 'text-center btn btn-success '),
                    'url'=>'Yii::app()->createUrl("admin/update/", array("id"=>$data->id))',
                    'click'=>'',
                ),

                'view' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class=" glyphicon glyphicon-eye-open"></span>',
                    'options' => array('title' => 'Просмотр', 'class' => 'text-center btn btn-info'),
                    'url'=>'Yii::app()->createUrl("admin/view", array("id"=>$data->id))',
                    'click'=>'',
                ),
                'pass' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class=" glyphicon glyphicon-envelope"></span>',
                    'options' => array('title' => 'Востановить пароль', 'class' => 'text-center btn btn-warning'),
                    'url'=>'Yii::app()->createUrl("admin/sendpassword", array("id"=>$data->id))',

                ),
                'delete' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class=" glyphicon glyphicon-remove-circle"></span>',
                    'options' => array('title' => 'Удалить', 'class' => 'text-center btn btn-danger'),
                    'url'=>'Yii::app()->createUrl("admin/delete", array("id"=>$data->id))',
                ),
            ),
        ),
	),
)); ?>
</div></div>