<?php
/* @var $this AdminController */
/* @var $model Admin */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'admin-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'role_id', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?=$form->dropDownList($model,'role_id',array(Admin::STATUS_USER=>'User',Admin::STATUS_MANAGER=>'Manager',Admin::STATUS_ADMIN=>'Admin'), array('class' => 'form-control'));?>
    </div>
    <?=$form->error($model, 'role_id',array('class'=>"label label-danger col-xs-10")); ?>
</div>
<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'login', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'login', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'login',array('class'=>"label label-danger col-xs-10")); ?>
</div>
<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'pass', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
        <?php echo $form->passwordField($model, 'pass', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'pass',array('class'=>"label label-danger col-xs-10")); ?>
</div>
<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'email', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'email', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'email',array('class'=>"label label-danger col-xs-10")); ?>
</div>
<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'description', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?php echo $form->textArea($model, 'description', array('class' => 'form-control','style' => 'height: 100%','rows'=>6, 'cols'=>50)); ?>
    </div>
    <?=$form->error($model, 'description',array('class'=>"label label-danger col-xs-10")); ?>
</div>
	<div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',array('class'=>'btn btn-success')); ?>
    </div>

<?php $this->endWidget(); ?>
