<?php
/* @var $this AdminController */
/* @var $model Admin */

$this->breadcrumbs=array(
	'Admins'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Управление сотрудниками', 'url'=>array('admin')),
);
?>
    <div class="col-xs-12 block">
        <div class="col-xs-12 block_header">
            <h1>Добавление сотрудника</h1>
        </div>
        <div class="col-xs-12 block_inner">

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
            </div></div>