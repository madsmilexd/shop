<?php
/* @var $this GoodsController */
/* @var $model Goods */

$this->breadcrumbs=array(
	'Goods'=>array('index'),
	'Create',
);
?>
<div class="jumbotron">
<h2>Создать товар</h2>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>

</div>