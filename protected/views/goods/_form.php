<?php
/* @var $this GoodsController */
/* @var $model Goods */
/* @var $form CActiveForm */
?>

<div class="form">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'goods-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation' => false,
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
	)); ?>

	<h4 class="note">Поля красного цвета обязательны для заполнения.</h4>


	<?=$form->errorSummary($model);?>

	<div class="row" style="margin-bottom: 13px;">

			<?

			$model3 = Category::model()->findAll();
			$menu=array();
			foreach($model3 as $category)
			{
				array_push($menu,array('id'=>$category->id,'name'=>$category->name, 'parent_id'=>$category->parent_id));
			}
			foreach($menu as $lol)
			{
				$data[$lol[id]] = $lol;
			}
			function view_cat ($dataset) {
				foreach ($dataset as $menu) {
					echo '<li><a href="'.Yii::app()->createAbsoluteUrl('site/by/'.$menu['id']).'">'.$menu["name"].'</a>';
					if($menu['childs']) {
						echo '<ul>';
						view_cat($menu['childs']);
						echo '</ul>';
					}
					echo '</li>';
				}
			}
			function mapTree($dataset) {
				$tree = array();
				foreach ($dataset as $id=>&$node) {
					if (!$node['parent_id']) {
						$tree[$id] = &$node;
					}
					else {
						$dataset[$node['parent_id']]['childs'][$id] = &$node;
					}
				}
				return $tree;
			}
			// вызываем функцию и передаем ей наш массив
			$data = mapTree($data);

			echo "<div id='ctree' class='text-left'>";
			echo '<ul>';
			view_cat($data);
			echo '</ul>';
			echo "</div><hr/>";
			?>
		<div class="input-group col-xs-6">
			<span class="input-group-addon alert-danger" style="width: 40%">Категория</span>
			<?=$form->dropDownList($model,'category_id',Category::model()->getAll(), array('maxlength' => 255,
				'class' => 'form-control',
				'style' => 'height: 100%'));
			?>
		</div>
		<?=$form->error($model, 'category_id',array('class'=>"label label-danger col-xs-6")); ?>
	</div>

	<? if($model->getIsNewRecord()){?>
	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-6">
			<span class="input-group-addon alert-danger" style="width: 40%">Код товара</span>
			<?php echo $form->textField($model, 'code', array('maxlength' => 255, 'class' => 'form-control',
				'style' => 'height: 100%')); ?>
		</div>
		<?=$form->error($model, 'code',array('class'=>"label label-danger col-xs-6")); ?>
	</div>
	<?}?>

	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-6">
			<span class="input-group-addon alert-danger" style="width: 40%">Название</span>
			<?php echo $form->textField($model, 'name', array('maxlength' => 255, 'class' => 'form-control',
				'style' => 'height: 100%')); ?>
		</div>
		<?=$form->error($model, 'name',array('class'=>"label label-danger col-xs-6")); ?>
	</div>


	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-6">
			<span class="input-group-addon alert-danger" style="width: 40%">Цена</span>
			<?php echo $form->numberField($model, 'pricex100', array('maxlength' => 255,
				 'class' => 'form-control','pattern'=>'\d+',
				'style' => 'height: 100%')); ?><span class="input-group-addon">руб.</span>
		</div>
		<?=$form->error($model, 'pricex100',array('class'=>"label label-danger col-xs-6")); ?>
	</div>

	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-6">
			<span class="input-group-addon alert-danger" style="width: 40%">Статус</span>
			<?php /*echo $form->textField($model, 'status', array('maxlength' => 255, 'class' => 'form-control',
				'style' => 'height: 100%')); */
			echo $form->dropDownList($model,'status',array(0=>'Нет на складе',1=>'Есть на складе',2=>'Под заказ'),
				array('maxlength' => 255,
				'class' => 'form-control',
				'style' => 'height: 100%'));?>
		</div>
		<?=$form->error($model, 'status',array('class'=>"label label-danger col-xs-6")); ?>
	</div>


	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-6">
			<span class="input-group-addon alert-danger" style="width: 40%">Количество</span>
			<?php echo $form->numberField($model, 'count', array('maxlength' => 255, 'class' => 'form-control',
				'pattern'=>'\d+','style' => 'height: 100%')); ?><span class="input-group-addon">шт.</span>
		</div>
		<?=$form->error($model, 'count',array('class'=>"label label-danger col-xs-6")); ?>
	</div>


	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-6">
			<span class="input-group-addon alert-success" style="width: 40%">Номер склада</span>
			<?=$form->dropDownList($model,'warehouse_id',Warehouse::model()->getAll(), array('maxlength' => 255, 'class' => 'form-control',
				'style' => 'height: 100%'));
			?>
		</div>
		<?=$form->error($model, 'warehouse_id',array('class'=>"label label-danger col-xs-6")); ?>
	</div>




	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-6">
			<span class="input-group-addon alert-danger" style="width: 40%">Mete keyword</span>
			<?php echo $form->textField($model, 'kw', array('maxlength' => 255, 'class' => 'form-control',
				'style' => 'height: 100%')); ?>
		</div>
		<?=$form->error($model, 'kw',array('class'=>"label label-danger col-xs-6")); ?>
	</div>



	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-6">
			<span class="input-group-addon alert-danger" style="width: 40%">Meta description</span>
			<?php echo $form->textField($model, 'ds', array('maxlength' => 255, 'class' => 'form-control',
				'style' => 'height: 100%')); ?>
		</div>
		<?=$form->error($model, 'ds',array('class'=>"label label-danger col-xs-6")); ?>
	</div>
	<?

	echo $form->fileField($model, 'image');
	echo '<br/>';

	if (!$model->isNewRecord&&$model->path!='empty.png') {

		echo '<div class="input-group col-xs-6">
      <span class="input-group-addon">
        '.$form->checkBox($model, 'remove_img').'
      </span>
      <span type="text" class="input-group-addon alert alert-warning"> Удалить изображение?</span>
    </div>';


	}
	echo '<br/>';
	?>

	<span class="input-group-addon alert-danger" style="border-radius: 5px 5px 0 0">Характеристики</span>
	<?
	$this->widget(
		'bootstrap.widgets.TbCKEditor',
		array(
			'model' => $model,
			'attribute' => 'description',
		)
	);
	?>
	<? if($model->getError
		('description')){?>
	<span class="input-group-addon alert-danger" style="border-radius: 0 0 5px 5px">
			<?=$model->getError('description');?>
		</span>
	<?}?>

</div>





	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-success')); ?>
	</div>

	<?php $this->endWidget(); ?>


</div>