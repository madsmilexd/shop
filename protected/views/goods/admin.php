<?php
/* @var $this GoodsController */
/* @var $model Goods */

$this->breadcrumbs = array(
	'Goods' => array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#goods-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Goods</h1>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'goods-grid',
	'ajaxUpdate' => true,
	'dataProvider' => $model->search(),
    'summaryText'=>false,
    'columns' => array(
		array('name' => 'id',
			'header' => '#',
			'type' => 'html',
			'htmlOptions' => array('class' => 'alert-warning','style'=>'vertical-align:bottom'),
			'value' => function ($data) {
					return '<span class="col-xs-offset-5">'.$data->id.'</span>';
				},
		),
		array('name' => 'code',
			'type' => 'html',
			'htmlOptions' => array('class' => 'alert-info','style'=>'vertical-align:bottom'),
			'value' => function ($data) {
					return '<span class="col-xs-offset-2">'.$data->code.'</span>';
				},
		),
		array('name' => 'category_id',
			'type' => 'html',
			'htmlOptions' => array('class' => 'alert-warning','style'=>'vertical-align:bottom'),
			'value' => function ($data) {
					return '<span class="col-xs-offset-2">'.$data->category->name.'</span>';
				},
		),
		array('name' => 'name',
			'type' => 'html',
			'htmlOptions' => array('class' => 'alert-info','style'=>'vertical-align:bottom'),
			'value' => function ($data) {
					return '<span>'.$data->name.'</span>';
				},
		),
		array('name' => 'pricex100',
			'type' => 'html',
			'htmlOptions' => array('class' => 'alert-warning','style'=>'vertical-align:bottom'),
			'value' => function ($data) {
					return '<span>'.$data->pricex100.' руб. </span>';
				},
		),
		array('name' => 'status',
			'type' => 'html',
			'htmlOptions' => array('class' => 'alert-info','style'=>'vertical-align:bottom'),
			'value' => function ($data) {
					if($data->status==0)
					return '<span>Нет на складе</span>';
					if($data->status==1)
						return '<span>В наличии</span>';
					if($data->status==2)
						return '<span>Под заказ</span>';
				},
		),
		'count',
		array('name' => 'warehouse_id',
			'type' => 'html',
			'htmlOptions' => array('class' => 'alert-info','style'=>'vertical-align:bottom'),
			'value' => function ($data) {
					if($data->warehouse_id)
					return '<span>#'.$data->warehouse_id.'('.$data->warehouse->description.')</span>';
					else
						return 'Не указан';
				},
		),

		array(
			'header' => '',
			'class' => 'CButtonColumn',
			'template' => '{update}{view}{delete}',
			'htmlOptions' => array('class'=>'btn-group col-xs-3'),
			'buttons' => array
			(
				'update' => array
				(
					'imageUrl' => false,
					'label' => '<span type="button" class="glyphicon glyphicon-pencil"></span>',
					'options' => array('title' => 'edit', 'class' => 'text-center btn btn-success '),
					'url'=>'Yii::app()->createUrl("goods/update/", array("id"=>$data->id))',
					'click'=>'',
				),

				'view' => array
				(
					'imageUrl' => false,
					'label' => '<span type="button" class=" glyphicon glyphicon-eye-open"></span>',
					'options' => array('title' => 'view', 'class' => 'text-center btn btn-info'),
					'url'=>'Yii::app()->createUrl("site/view", array("id"=>$data->id))',
					'click'=>'',
				),

				'delete' => array
				(
					'imageUrl' => false,
					'label' => '<span type="button" class=" glyphicon glyphicon-remove-circle"></span>',
					'options' => array('title' => 'delete', 'class' => 'text-center btn btn-danger'),
					'url'=>'Yii::app()->createUrl("goods/delete", array("id"=>$data->id))',
				),
			),
		),
	),
)); ?>
