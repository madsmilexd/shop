<?php
/* @var $this GoodsController */
/* @var $data Goods */
?>

<div class="view">


	<table class="table table-striped table-responsive">
		<tr>
			<td class="success col-xs-3">ID: <?=$data->id;?></td>
			<td class="info">Код товара: <?=$data->code;?></td>
			<td class="success">Категория: <?=$data->category->name;?></td>
		</tr>
		<tr>
			<td class="success">Наименование: <?=$data->name;?></td>
			<td class="info">Путь к папке с картинками: <?=$data->path;?></td>
			<td class="success">Цена товара: <?=$data->pricex100;?></td>
		</tr>
		<tr>
			<td class="success">Статус: <?=($data->status==0)?'Нет на складе':(($data->status==1)?'Есть на
			складе':'Под заказ');?></td>
			<td class="info">Количество: <?=$data->count;?></td>
			<td class="success">Склад: <?=($data->warehouse_id)?$data->warehouse->description:'Не указан';?></td>
		</tr>
		<tr>
			<td class="success">Описание</td>
			<td colspan="2"><?=$data->description;?></td>
		</tr>
		<tr>
			<td colspan="3">
				<div class="btn-group col-xs-offset-10	">
					<a href="<?=Yii::app()->createAbsoluteUrl('goods/update/'.$data->id);?>" type="button" class="btn
btn-success
glyphicon
glyphicon-pencil"></a>
					<a href="<?=Yii::app()->createAbsoluteUrl('site/'.$data->id);?>" type="button" class="btn
					btn-info glyphicon glyphicon-eye-open"></a>
					<a href="<?=Yii::app()->createAbsoluteUrl('goods/delete	/'.$data->id);?>" type="button" class="btn
					btn-danger glyphicon
					glyphicon-remove-circle"></a>
				</div>
			</td>
		</tr>
	</table>

<hr/>
</div>