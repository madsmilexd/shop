<?php
/* @var $this GoodsController */
/* @var $model Goods */

$this->breadcrumbs=array(
	'Goods'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);
?>
<div class="jumbotron">
<?="<a href='".Yii::app()->createAbsoluteUrl('site/'.$model->id)."'>Открыть данный товар в каталоге</a>";
?>

<h2>Изменить товар: <?php echo $model->name.' ('.$model->code.')'; ?></h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>