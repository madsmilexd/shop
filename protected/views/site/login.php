<?php
/* @var UserController $this */
/* @var User $model */
/* @var CActiveForm $form */
?>

<div id="form_sign" class="col-sm-offset-3 col-sm-6">

	<?php

	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'user-login-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// See class documentation of CActiveForm for details on this,
		// you need to use the performAjaxValidation()-method described there.
		'enableAjaxValidation' => false,
	)); ?>


	<?= $form->errorSummary($model); ?>

	<div class="form-group">
			<?= $form->textField($model, 'email', array('class' => 'form-control','style'=>"height:20%",
				'placeholder' => 'Enter e-mail',
				'type' => 'email', 'required' => 'required')); ?>

	</div>


	<div class="form-group">

			<?= $form->passwordField($model, 'pass', array('class' => 'form-control','style'=>"height:20%",
				'placeholder' => 'Enter password', 'required' => 'required')); ?>

	</div>
	<div class="form-group">
		<div class="col-xs-12 text-right">
			<?= '<a href="' . $this->createUrl('/mailur/recovery/') . '">Forgot password?</a>'; ?>
		</div>
	</div>

	<div class="form-group text-center">

		<?= CHtml::submitButton('Submit', array('class' => 'btn btn-primary btn-lg')); ?>

	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->