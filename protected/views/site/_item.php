<?php
/* @var SiteController $this */
/* @var array $error */
/* @var Goods $item */
?>

<div class="col-sm-6 col-lg-4 goods_item">

	<h2><?='#'.$item->id.' '.$item->name;?></h2>
	<sup><?=$item->code;?></sup><br/>
	<?=$item->getPrevImage();?>
	<p><?
		if(strlen($item->description)>=200){
			echo substr($item->description,0,200).'...';
		}else{
			echo $item->description;
		}
		?></p>
	<p><?=$item->getPriceLabel();?></p>
	<p><?=$item->getStatus();?></p>
	<p><a class="btn btn-default" href="<?=Yii::app()->createAbsoluteUrl('site/view/'.$item->id)?>"
	role="button">Подробней
	»</a>

	<? if($item->status!=0){?>
	<a class="btn btn-success" href="<?=Yii::app()->createAbsoluteUrl('site/put/'.$item->id)?>"
		  role="button">Заказать</a>
	<?}?></p>
</div>