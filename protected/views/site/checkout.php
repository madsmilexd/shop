<?php
/* @var $this OrdersController */
/* @var $model Orders */
/* @var $form CActiveForm */



$items = Yii::app()->ShoppingCart->getItems();
?>



<div class="jumbotron">
	<h2>Оформление заказа</h2>

	<?php $this->renderPartial('_checkout', array('model'=>$model,'items'=>$items)); ?>
</div>