<?php
/* @var UserController $this */
/* @var User $model */
/* @var CActiveForm $form */

if(!$status){
?>

<form role="form" class="col-sm-8 col-xs-offset-2" method="post">
	<div class="form-group col-xs-6">
		<input name="Mail[name]" id="nameHolder" type="text" class="form-control col-xs-6" placeholder="Введите Ф.И.О"
			   style="height:
		30px"
			   required="required">
	</div>
	<div class="clearfix"></div>
	<div class="form-group col-xs-6">
		<input name="Mail[email]" type="email" id="emailHolder" class="form-control col-xs-6" placeholder="Введите
		адрес электронной почты" style="height:
		30px" required="required">
	</div>
	<div class="form-group col-xs-6">
		<input name="Mail[tel]" type="text" class="form-control col-xs-6" placeholder="Введите номер телефона"
			   style="height: 30px">
	</div>
	<div class="form-group col-xs-12">
		<textarea name="Mail[message]" id="textHolder" class="form-control" style="width: 100%" rows="10"
				  required="required" placeholder="Ваше сообщение.."></textarea>
	</div>

	<div class="form-group col-xs-12">
		<button type="submit" id="submitMail" class="btn btn-info" disabled="disabled">Отправить</button>
	</div>
</form>
	<script>
		$(document).keyup(function(){
			if($('#nameHolder').val().length>1&&$('#emailHolder').val().length>1&&$('#textHolder').val().length>1){
				$('#submitMail').removeAttr('disabled');
			}
		});
	</script>


<?}else{
	if($status=='fail'){
?>
		<div class="alert alert-danger alert-dismissable text-center" style="width: 50%;
margin: 20% auto;">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true"
					onclick="closeIt(this)">&times;</button>
			<h2>Ошибка!</h2>
			При отправке сообщения произошла ошибка, пожалуйста свяжитесь с нами по телефону <?=Yii::app()
			->params['shopPhone'];?>
		</div>

	<?
	}

	if($status=='success'){
		?>
		<div class="alert alert-success alert-dismissable text-center" style="width: 50%;
margin: 20% auto;">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true"
					onclick="closeIt(this)">&times;</button>
			<h2>Отправленно!</h2>
			Сообщение успешно отправленно, мы свяжеся с вами в ближайшее время
		</div>

	<?
	}



}