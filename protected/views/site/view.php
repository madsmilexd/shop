<?php
/**
 * @var SiteController $this
 * @var Post[] $posts
 */
/** @var Goods $item */
if (isset($item)&&(!isset($error))) {
	?>
		<h2><?=(Yii::app()->user->checkAccess(Admin::STATUS_MANAGER))?'<a href="'.Yii::app()
					->createAbsoluteUrl('goods/update/'.$item->id).'" class="btn btn-success"><span class="glyphicon
					glyphicon-pencil"></span>&nbsp;'.$item->name.'</a>':$item->name?></h2>
			<?=$item->getImage();?>

	<p>Код товара: <?=$item->code;?></p>
	<p>Категория товара: <?=$item->category->name;?>,</p>
	<p>Название товара: <?=$item->name;?></p>
	<p>Характеристики: <?=$item->description;?></p>
	<p>Цена:<?=$item->pricex100;
		?></p>
	<p><?=$item->getStatus();?></p>
	<? if($item->status!=0){?>
		<p><a class="btn btn-success" href="<?=Yii::app()->createAbsoluteUrl('site/put/'.$item->id)?>"
			  role="button">Заказать</a></p>
	<?}?>
	<?
}


if (isset($error)) {

	?>
	<div class="alert alert-danger alert-dismissable text-center" style="width: 50%;
margin: 20% auto;">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true"
				onclick="closeIt(this)">&times;</button>
		<h2>Ошибочка</h2>
		Товар не найден
	</div>
<?
}

?>


