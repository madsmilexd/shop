<?php
/* @var $this OrdersController */
/* @var $model Orders */
/* @var $form CActiveForm */
?>

<div class="form">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'orders-_checkout-form',
		'enableAjaxValidation' => false,
	)); ?>

	<h4 class="note">Поля красного цвета обязательны для заполнения.</h4>


	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-8 col-xs-offset-2">
			<span class="input-group-addon alert-danger" style="width: 40%"><?=
				$model->isNewRecord ? 'Ваше имя ' : 'Имя
			 заказчика';?></span>
			<?php echo $form->textField($model, 'client_name', array('maxlength' => 255, 'class' => 'form-control',
				'style' => 'height: 100%')); ?>
		</div>
		<?= $form->error($model, 'client_name', array('class' => "label label-danger col-xs-8 col-xs-offset-2")); ?>
	</div>

	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-8 col-xs-offset-2">
			<span class="input-group-addon alert-danger" style="width: 40%"><?= $model->isNewRecord ? 'Ваш ' : ''; ?>
				email</span>
			<?php echo $form->emailField($model, 'client_email', array('maxlength' => 255, 'class' => 'form-control',
				'style' => 'height: 100%')); ?>
		</div>
		<?= $form->error($model, 'client_email', array('class' => "label label-danger col-xs-8 col-xs-offset-2")); ?>
	</div>

	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-8 col-xs-offset-2">
			<span class="input-group-addon alert-danger" style="width: 40%">Номер телефона</span>
			<?php echo $form->textField($model, 'client_phone', array('maxlength' => 255, 'class' => 'form-control',
				'style' => 'height: 100%')); ?>
		</div>
		<?= $form->error($model, 'client_phone', array('class' => "label label-danger col-xs-8 col-xs-offset-2")); ?>
	</div>


	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-8 col-xs-offset-2">
			<span class="input-group-addon alert-danger" style="width: 40%">Способ оплаты</span>
			<?php echo $form->textField($model, 'payment', array('maxlength' => 255, 'class' => 'form-control',
				'style' => 'height: 100%','value'=>'Наличные','disabled'=>'disabled')); ?>
		</div>
		<?= $form->error($model, 'payment', array('class' => "label label-danger col-xs-8 col-xs-offset-2")); ?>
	</div>


	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-8 col-xs-offset-2">
			<span class="input-group-addon alert-danger" style="width: 40%">Способ доставки</span>
			<?=
			$form->dropDownList($model, 'delivery', array(0 => 'Самовывоз', 1 => 'Доставка'),
				array('maxlength' => 255,
					'class' => 'form-control',
					'style' => 'height: 100%'));?>
		</div>
		<?= $form->error($model, 'delivery', array('class' => "label label-danger col-xs-8 col-xs-offset-2")); ?>
	</div>

	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-8 col-xs-offset-2">
			<span class="input-group-addon alert-info" style="width: 40%">Адрес</span>
			<?php echo $form->textField($model, 'address', array('maxlength' => 255, 'class' => 'form-control',
				'style' => 'height: 100%')); ?>
		</div>
		<?= $form->error($model, 'address', array('class' => "label label-danger col-xs-8 col-xs-offset-2")); ?>
	</div>


	<?

	if (!$items) {
		echo '<a href="/" class="btn btn-success glyphicon">Ваша корзина пуса, вернуться в каталог <span class="glyphicon-shopping-cart"></span> </a>';
	}else{
		foreach ($items as $item) {
			$item = Goods::model()->findByPk($item->id);
			?>
	<div class="row" style="margin-bottom: 13px;">
			<div class="input-group col-xs-8 col-xs-offset-2">
				<a href="<?=Yii::app()->createAbsoluteUrl('site/view/'.$item->id);?>" class="input-group-addon
				alert-success"
				><span class="glyphicon glyphicon-eye-open"></span> </a>
				<input value="<?=$item->name.'('
				.$item->code
				.')';
				?>" style="height:
    100%"  class="form-control"/>
				<input name="count" data-forinputid="goods<?=$item->id?>" data-price="<?=$item->pricex100;?>"
					   data-goodsid="<?=$item->id?>"
					   class="form-control" pattern="\d+" style="height:
				100%"
					   max="<?=$item->count;?>"
					   min="0"
				type="number" value="1" >
				<span class="input-group-addon alert-success">шт.</span>
				</div>
				<div class="input-group col-xs-8 col-xs-offset-2">
				<span class="input-group-addon alert-danger">Итого: </span>
				<input class="form-control" id="goods<?=$item->id?>" disabled="disabled" style="height: 100%"
					   type="text"
				value="<?
				$price = $item->pricex100;
					echo ($price).' руб.';
				?>">
			</div>
		</div>



	<?
		}
		?>

		<div class="row" style="margin-bottom: 13px;">
			<div class="input-group col-xs-8 col-xs-offset-2">
				<span class="input-group-addon alert-danger" style="width: 40%">Всего: </span>
				<input class="form-control" id="totaly" disabled="disabled" style="height: 100%" type="text" value="<?
				$total = 0;
				foreach($items as $item){
					$total += $item->pricex100;
				}
				echo ($total);
				?>">
				<span class="input-group-addon alert-danger" style="width: 5%">руб.</span>
			</div>
		</div>
	<?
	}
	?>



	<div class="row hidden">
		<?php echo $form->textField($model, 'goods'); ?>
	</div>

	<div class="row	hidden">

		<?php echo $form->textField($model, 'sum_total',array('value'=>$total)); ?>

	</div>


	<div class="row buttons">
		<?=CHtml::submitButton($model->isNewRecord ? 'Заказать' : 'Сохранить',
			array('class' => 'btn btn-success
		col-xs-offset-6
		'));
		echo $form->errorSummary($model);
		?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->