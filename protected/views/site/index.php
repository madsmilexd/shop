<?php
/**
 * @var SiteController $this
 * @var Post[] $posts
 */


if (isset($goods)) {
	$i = 0;
	foreach ($goods as $item) {
		$this->renderPartial('_item',array('item'=>$item));
	}
}


if (isset($pages)) {
	echo '<div class="col-xs-12 text-center">';
	$this->widget('CLinkPager', array(
		'pages' => $pages,
		'maxButtonCount' => 5, // максимальное вол-ко кнопок <- 1..2..3..4..5 ->
		'header' => ' ', //'<b>Перейти к странице:</b><br><br>', // заголовок над листалкой
		'firstPageLabel' => '<div style="height:17px;width:20px" class="glyphicon glyphicon-fast-backward"></div>',
		'prevPageLabel' => '<div style="height:17px;width:20px"  class="glyphicon glyphicon-backward"></div>',
		'nextPageLabel' => '<div style="height:17px;width:20px" class="glyphicon glyphicon-forward"></div>',
		'lastPageLabel' => '<div style="height:17px;width:20px" class="glyphicon glyphicon-fast-forward"></div>',
		'htmlOptions' => array('class' => 'pagination pagination-sm'),
		'selectedPageCssClass' => 'active'
	));
	echo '</div>';
}

?>


