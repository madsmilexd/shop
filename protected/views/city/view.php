<?php
/* @var $this CityController */
/* @var $model City */

$this->breadcrumbs=array(
	'Cities'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Создать город', 'url'=>array('create')),
	array('label'=>'Редактировать город', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить город', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы действительно хотите удалить?')),
	array('label'=>'Управление городами', 'url'=>array('admin')),
);
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Просмотр города №<?php echo $model->id; ?></h1>
    </div>
    <div class="col-xs-12 block_inner">

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
</div></div>