<?php
/* @var $this CityController */
/* @var $model City */

$this->breadcrumbs=array(
	'Cities'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Создать город', 'url'=>array('create')),
	array('label'=>'Просмотр города', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление городами', 'url'=>array('admin')),
);
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Редактировать города №<?php echo $model->id; ?></h1>
    </div>
    <div class="col-xs-12 block_inner">


<?php $this->renderPartial('_form', array('model'=>$model)); ?>
        </div></div>