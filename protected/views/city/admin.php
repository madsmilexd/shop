<?php
/* @var $this CityController */
/* @var $model City */

$this->breadcrumbs=array(
	'Cities'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Создать город', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#city-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Управление городами</h1>

    </div>
    <div class="col-xs-12 block_inner">

<?php echo CHtml::link('Продвинутый поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'city-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'summaryText'=>false,
//	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
        array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => '{update}{view}{delete}',
            'htmlOptions' => array('class'=>'btn-group col-xs-2'),
            'buttons' => array
            (
                'update' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class="glyphicon glyphicon-pencil"></span>',
                    'options' => array('title' => 'Редактировать', 'class' => 'text-center btn btn-success '),
                    'url'=>'Yii::app()->createUrl("city/update/", array("id"=>$data->id))',
                    'click'=>'',
                ),

                'view' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class=" glyphicon glyphicon-eye-open"></span>',
                    'options' => array('title' => 'Просмотр', 'class' => 'text-center btn btn-info'),
                    'url'=>'Yii::app()->createUrl("city/view", array("id"=>$data->id))',
                    'click'=>'',
                ),

                'delete' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class=" glyphicon glyphicon-remove-circle"></span>',
                    'options' => array('title' => 'Удалить', 'class' => 'text-center btn btn-danger'),
                    'url'=>'Yii::app()->createUrl("city/delete", array("id"=>$data->id))',
                ),
            ),
        ),
	),
)); ?>
        </div></div>
