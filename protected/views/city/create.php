<?php
/* @var $this CityController */
/* @var $model City */

$this->breadcrumbs=array(
	'Cities'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Управление городами', 'url'=>array('admin')),
);
?>
    <div class="col-xs-12 block">
        <div class="col-xs-12 block_header">
            <h1>Создание города</h1>

        </div>
        <div class="col-xs-12 block_inner">


<?php $this->renderPartial('_form', array('model'=>$model)); ?>
            </div></div>