<?php
/* @var $this CityController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cities',
);

$this->menu=array(
	array('label'=>'Create City', 'url'=>array('create')),
);
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Cities</h1>
    </div>
    <div class="col-xs-12 block_inner">


<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
</div></div>