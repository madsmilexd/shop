<?php
/* @var $this OrdersController */
/* @var $model Orders */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Управление заказами', 'url'=>array('admin')),
);
?>
    <div class="col-xs-12 block">
        <div class="col-xs-12 block_header">
            <h1>Создание заказа</h1>
        </div>
        <div class="col-xs-12 block_inner">

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
            </div></div>