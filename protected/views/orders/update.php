<?php
/* @var $this OrdersController */
/* @var $model Orders */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Создать заказ', 'url'=>array('create')),
	array('label'=>'Просмотр заказа', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление заказами', 'url'=>array('admin')),
);
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Редактирование заказа №<?php echo $model->id; ?></h1>
    </div>
    <div class="col-xs-12 block_inner">

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
        </div></div>