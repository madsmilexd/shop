<?php
/* @var $this OrdersController */
/* @var $model Orders */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'orders-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>


	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-10">
			<?=$form->labelEx($model,'client_name', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
			<?php echo $form->textField($model, 'client_name', array('disabled'=>'disabled','maxlength' => 255,
			'class' => 'form-control','style' => 'height: 100%')); ?>
		</div>
		<?=$form->error($model, 'client_name',array('class'=>"label label-danger col-xs-10")); ?>
	</div>


	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-10">
			<?=$form->labelEx($model,'client_email', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
			<?php echo $form->textField($model, 'client_email', array('disabled'=>'disabled','maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
		</div>
		<?=$form->error($model, 'client_email',array('class'=>"label label-danger col-xs-10")); ?>
	</div>

	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-10">
			<?=$form->labelEx($model,'client_phone', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
			<?php echo $form->textField($model, 'client_phone', array('disabled'=>'disabled','maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
		</div>
		<?=$form->error($model, 'client_phone',array('class'=>"label label-danger col-xs-10")); ?>
	</div>


	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-10">
			<?=$form->labelEx($model,'sum_total', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
			<?php echo $form->textField($model, 'sum_total', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
		</div>
		<?=$form->error($model, 'sum_total',array('class'=>"label label-danger col-xs-10")); ?>
	</div>


		<div class="row" style="margin-bottom: 13px;">
			<div class="input-group col-xs-10">
				<?=$form->labelEx($model,'delivery', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
				<?=$form->dropDownList($model, 'delivery',$model->getDelivery() , array('class' => 'form-control'));?>
			</div>
			<?=$form->error($model, 'delivery',array('class'=>"label label-danger col-xs-10")); ?>
		</div>



	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-10">
			<?=$form->labelEx($model,'payment', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
			<?php echo $form->textField($model, 'payment', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
		</div>
		<?=$form->error($model, 'payment',array('class'=>"label label-danger col-xs-10")); ?>
	</div>


	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-10">
			<?=$form->labelEx($model,'address', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
			<?php echo $form->textField($model, 'address', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
		</div>
		<?=$form->error($model, 'address',array('class'=>"label label-danger col-xs-10")); ?>
	</div>



	<div class="row" style="margin-bottom: 13px;">
		<div class="input-group col-xs-10">
			<?=$form->labelEx($model,'status', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
			<?=$form->dropDownList($model, 'status',$model->getStatus()	 , array('class' => 'form-control'));?>
		</div>
		<?=$form->error($model, 'status',array('class'=>"label label-danger col-xs-10")); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',array('class'=>'btn btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>