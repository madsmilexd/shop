<?php
/* @var $this OrdersController */
/* @var $model Orders */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#orders-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Управление заказами</h1>
    </div>
    <div class="col-xs-12 block_inner">
<?php echo CHtml::link('Продвинутый поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'orders-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'summaryText'=>false,
    'columns'=>array(
		'id',
		'client_name',
		'client_email',
		'client_phone',
		array(
			'name'=>'goods',
			'type'=>'html',
			'value'=>function($data){
					$res = $data->parseOrder($data->goods);
					$result = '';
					/** @var Goods $item */
					foreach($res as $item){
						$g = Goods::model()->findByPk($item->id);
						$result .= '#'.$item->id. ' '.$g->name.'('.$item->count.'шт за '.($item->pricex100).'
						руб. каждый)<br/>';
					}
					return $result;
				}
		),
		array(
			'name'=>'sum_total',
			'type'=>'html',
			'value'=>function($data){
					return ($data->sum_total);
				}
		),
		array(
			'name'=>'delivery',
			'type'=>'html',
			'value'=>function($data){
					return $data->getDeliveryLabel($data->delivery);
				}
		),
		'payment',
		'address',
		array(
			'name'=>'status',
			'type'=>'html',
			'value'=>function($data){
					return $data->getStatusLabel($data->status);
				}
		),
		'created_at',
		'updated_at',

        array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => '{update}{view}{delete}',
            'htmlOptions' => array('class'=>'btn-group col-xs-2'),
            'buttons' => array
            (
                'update' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class="glyphicon glyphicon-pencil"></span>',
                    'options' => array('title' => 'Редактировать', 'class' => 'text-center btn btn-success '),
                    'url'=>'Yii::app()->createUrl("orders/update/", array("id"=>$data->id))',
                    'click'=>'',
                ),

                'view' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class=" glyphicon glyphicon-eye-open"></span>',
                    'options' => array('title' => 'Просмотр', 'class' => 'text-center btn btn-info'),
                    'url'=>'Yii::app()->createUrl("orders/view", array("id"=>$data->id))',
                    'click'=>'',
                ),

                'delete' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class=" glyphicon glyphicon-remove-circle"></span>',
                    'options' => array('title' => 'Удалить', 'class' => 'text-center btn btn-danger'),
                    'url'=>'Yii::app()->createUrl("orders/delete", array("id"=>$data->id))',
                ),
            ),
        ),
	),
)); ?>
</div></div>