<?php
/* @var $this OrdersController */
/* @var $model Orders */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Редактировать заказ', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить заказ', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы действительно хотите удалить?')),
	array('label'=>'Управление заказами', 'url'=>array('admin')),
);
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Просмотр заказа №<?php echo $model->id; ?></h1>
    </div>
    <div class="col-xs-12 block_inner">

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'client_name',
		'client_email',
		'client_phone',
		array(
			'name'=>'goods',
			'type'=>'html',
			'value'=>function($data){
					$res = $data->parseOrder($data->goods);
					$result = '';
					/** @var Goods $item */
					foreach($res as $item){
						$g = Goods::model()->findByPk($item->id);
						$result .= '#'.$item->id. ' '.$g->name.'('.$item->count.'шт за '.($item->pricex100).'
						руб. каждый)<br/>';
					}
					return $result;
				}
		),
		'sum_total',
		array(
			'name'=>'delivery',
			'type'=>'html',
			'value'=>function($data){
					return $data->getDeliveryLabel($data->delivery);
				}
		),
		'payment',
		'address',
		array(
			'name'=>'status',
			'type'=>'html',
			'value'=>function($data){
					return $data->getStatusLabel($data->status);
				}

		),
		'created_at',
		'updated_at',
	),
)); ?>
        </div></div>
