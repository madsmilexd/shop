<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Category', 'url'=>array('admin')),
);
?>
    <div class="col-xs-12 block">
        <div class="col-xs-12 block_header">
            <h1>Создание категории</h1>
        </div>
        <div class="col-xs-12 block_inner">

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
            </div></div>