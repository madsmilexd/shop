<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'category-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'name', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'name', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'name',array('class'=>"label label-danger col-xs-10")); ?>
    </div>

	<div class="row" style="margin-bottom: 13px;">

		<?

		$model3 = Category::model()->findAll();
		$menu=array();
		foreach($model3 as $category)
		{
			array_push($menu,array('id'=>$category->id,'name'=>$category->name, 'parent_id'=>$category->parent_id));
		}
		foreach($menu as $lol)
		{
			$data[$lol[id]] = $lol;
		}
		function view_cat ($dataset) {
			foreach ($dataset as $menu) {
				echo '<li><a href="'.Yii::app()->createAbsoluteUrl('site/by/'.$menu['id']).'">'.$menu["name"].'</a>';
				if($menu['childs']) {
					echo '<ul>';
					view_cat($menu['childs']);
					echo '</ul>';
				}
				echo '</li>';
			}
		}
		function mapTree($dataset) {
			$tree = array();
			foreach ($dataset as $id=>&$node) {
				if (!$node['parent_id']) {
					$tree[$id] = &$node;
				}
				else {
					$dataset[$node['parent_id']]['childs'][$id] = &$node;
				}
			}
			return $tree;
		}
		// вызываем функцию и передаем ей наш массив
		$data = mapTree($data);

		echo "<div id='ctree' class='text-left'>";
		echo '<ul>';
		view_cat($data);
		echo '</ul>';
		echo "</div><hr/>";
		?>
		
		<div class="input-group col-xs-10">
			<?=$form->labelEx($model,'parent_id', array('class'=>'input-group-addon alert-info',
				'style'=>'width: 30%')); ?>
			<?php echo $form->dropDownList($model,'parent_id',Category::getArray(),array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
		</div>
		<?=$form->error($model, 'name',array('class'=>"label label-danger col-xs-10")); ?>
	</div>

	<div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',array('class'=>'btn btn-success')); ?>
    </div>

<?php $this->endWidget(); ?>