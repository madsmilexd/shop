<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Создание категории', 'url'=>array('create')),
	array('label'=>'Просмотре категории', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление категориями', 'url'=>array('admin')),
);
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Редактирование категории №<?php echo $model->id; ?></h1>
    </div>
    <div class="col-xs-12 block_inner">


<?php $this->renderPartial('_form', array('model'=>$model)); ?>
        </div></div>