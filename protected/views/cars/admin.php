<?php
/* @var $this CarsController */
/* @var $model Car */

$this->breadcrumbs=array(
	'Cars'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Создать машину', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#car-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Управление машинами</h1>

    </div>
    <div class="col-xs-12 block_inner">

<?php echo CHtml::link('Продвинутый поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'car-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'summaryText'=>false,
//	'filter'=>$model,
	'columns'=>array(
		'id',
		'user_id',
		'VIN',
		'brand',
		'marka',
		'specifications',
		/*
		'issue_year',
		'engine_number',
		'engine_volume',
		'color',
		'anti_block',
		'anti_block_comment',
		'anti_slip',
		'anti_slip_comment',
		'sensor',
		'sensor_comment',
		'conditioner',
		'conditioner_comment',
		'climat_control',
		'climat_control_comment',
		'rain_sensor',
		'rain_sensor_comment',
		'xenon',
		'xenon_comment',
		'course_stabilizee',
		'course_stabilizee_comment',
		'seat_heating',
		'seat_heating_comment',
		'hatch',
		'hatch_comment',
		'PDS',
		'PDS_comment',
		'multimedia',
		'multimedia_comment',
		'electro_elevator',
		'electro_elevator_comment',
		'electro_mirors',
		'electro_mirors_comment',
		'mirrors_heating',
		'mirrors_heating_comment',
		'additional_equipment',
		'out_of_equipment',
		'show',
		*/
        array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => '{update}{view}{delete}',
            'htmlOptions' => array('class'=>'btn-group col-xs-2'),
            'buttons' => array
            (
                'update' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class="glyphicon glyphicon-pencil"></span>',
                    'options' => array('title' => 'Редактировать', 'class' => 'text-center btn btn-success '),
                    'url'=>'Yii::app()->createUrl("cars/update/", array("id"=>$data->id))',
                    'click'=>'',
                ),

                'view' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class=" glyphicon glyphicon-eye-open"></span>',
                    'options' => array('title' => 'Просмотр', 'class' => 'text-center btn btn-info'),
                    'url'=>'Yii::app()->createUrl("cars/view", array("id"=>$data->id))',
                    'click'=>'',
                ),

                'delete' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class=" glyphicon glyphicon-remove-circle"></span>',
                    'options' => array('title' => 'Удалить', 'class' => 'text-center btn btn-danger'),
                    'url'=>'Yii::app()->createUrl("cars/delete", array("id"=>$data->id))',
                ),
            ),
        ),
	),
)); ?>
        </div></div>
