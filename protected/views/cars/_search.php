<?php
/* @var $this CarsController */
/* @var $model Car */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'VIN'); ?>
		<?php echo $form->textField($model,'VIN',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'brand'); ?>
		<?php echo $form->textField($model,'brand',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'marka'); ?>
		<?php echo $form->textField($model,'marka',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'specifications'); ?>
		<?php echo $form->textField($model,'specifications',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'issue_year'); ?>
		<?php echo $form->textField($model,'issue_year'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'engine_number'); ?>
		<?php echo $form->textField($model,'engine_number',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'engine_volume'); ?>
		<?php echo $form->textField($model,'engine_volume',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'color'); ?>
		<?php echo $form->textField($model,'color',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'anti_block'); ?>
		<?php echo $form->textField($model,'anti_block',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'anti_block_comment'); ?>
		<?php echo $form->textField($model,'anti_block_comment',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'anti_slip'); ?>
		<?php echo $form->textField($model,'anti_slip',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'anti_slip_comment'); ?>
		<?php echo $form->textField($model,'anti_slip_comment',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sensor'); ?>
		<?php echo $form->textField($model,'sensor',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sensor_comment'); ?>
		<?php echo $form->textField($model,'sensor_comment',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'conditioner'); ?>
		<?php echo $form->textField($model,'conditioner',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'conditioner_comment'); ?>
		<?php echo $form->textField($model,'conditioner_comment',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'climat_control'); ?>
		<?php echo $form->textField($model,'climat_control',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'climat_control_comment'); ?>
		<?php echo $form->textField($model,'climat_control_comment',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rain_sensor'); ?>
		<?php echo $form->textField($model,'rain_sensor',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rain_sensor_comment'); ?>
		<?php echo $form->textField($model,'rain_sensor_comment',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xenon'); ?>
		<?php echo $form->textField($model,'xenon',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xenon_comment'); ?>
		<?php echo $form->textField($model,'xenon_comment',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'course_stabilizee'); ?>
		<?php echo $form->textField($model,'course_stabilizee',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'course_stabilizee_comment'); ?>
		<?php echo $form->textField($model,'course_stabilizee_comment',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'seat_heating'); ?>
		<?php echo $form->textField($model,'seat_heating',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'seat_heating_comment'); ?>
		<?php echo $form->textField($model,'seat_heating_comment',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hatch'); ?>
		<?php echo $form->textField($model,'hatch',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hatch_comment'); ?>
		<?php echo $form->textField($model,'hatch_comment',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PDS'); ?>
		<?php echo $form->textField($model,'PDS',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PDS_comment'); ?>
		<?php echo $form->textField($model,'PDS_comment',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'multimedia'); ?>
		<?php echo $form->textField($model,'multimedia',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'multimedia_comment'); ?>
		<?php echo $form->textField($model,'multimedia_comment',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'electro_elevator'); ?>
		<?php echo $form->textField($model,'electro_elevator',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'electro_elevator_comment'); ?>
		<?php echo $form->textField($model,'electro_elevator_comment',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'electro_mirors'); ?>
		<?php echo $form->textField($model,'electro_mirors',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'electro_mirors_comment'); ?>
		<?php echo $form->textField($model,'electro_mirors_comment',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mirrors_heating'); ?>
		<?php echo $form->textField($model,'mirrors_heating',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mirrors_heating_comment'); ?>
		<?php echo $form->textField($model,'mirrors_heating_comment',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'additional_equipment'); ?>
		<?php echo $form->textField($model,'additional_equipment',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'out_of_equipment'); ?>
		<?php echo $form->textField($model,'out_of_equipment',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'show'); ?>
		<?php echo $form->textField($model,'show'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->