<?php
/* @var $this CarsController */
/* @var $model Car */

$this->breadcrumbs=array(
	'Cars'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Управление машинами', 'url'=>array('admin')),
);
?>
    <div class="col-xs-12 block">
        <div class="col-xs-12 block_header">
            <h1>Создание машины</h1>
        </div>
        <div class="col-xs-12 block_inner">

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
            </div></div>