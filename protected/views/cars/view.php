<?php
/* @var $this CarsController */
/* @var $model Car */

$this->breadcrumbs=array(
	'Cars'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Создание машины', 'url'=>array('create')),
	array('label'=>'Редактирование машины', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить машину', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы действительно хотите удалить?')),
	array('label'=>'Управление машинами', 'url'=>array('admin')),
);
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Просмотр машины #<?php echo $model->id; ?></h1>
    </div>
    <div class="col-xs-12 block_inner">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'VIN',
		'brand',
		'marka',
		'specifications',
		'issue_year',
		'engine_number',
		'engine_volume',
		'color',
		'anti_block',
		'anti_block_comment',
		'anti_slip',
		'anti_slip_comment',
		'sensor',
		'sensor_comment',
		'conditioner',
		'conditioner_comment',
		'climat_control',
		'climat_control_comment',
		'rain_sensor',
		'rain_sensor_comment',
		'xenon',
		'xenon_comment',
		'course_stabilizee',
		'course_stabilizee_comment',
		'seat_heating',
		'seat_heating_comment',
		'hatch',
		'hatch_comment',
		'PDS',
		'PDS_comment',
		'multimedia',
		'multimedia_comment',
		'electro_elevator',
		'electro_elevator_comment',
		'electro_mirors',
		'electro_mirors_comment',
		'mirrors_heating',
		'mirrors_heating_comment',
		'additional_equipment',
		'out_of_equipment',
		'show',
	),
)); ?>
</div></div>