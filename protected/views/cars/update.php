<?php
/* @var $this CarsController */
/* @var $model Car */

$this->breadcrumbs=array(
	'Cars'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Создание машины', 'url'=>array('create')),
	array('label'=>'Просмотр машины', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление машинами', 'url'=>array('admin')),
);
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Редактирование машины <?php echo $model->id; ?></h1>
    </div>
    <div class="col-xs-12 block_inner">

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
        </div></div>