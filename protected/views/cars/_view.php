<?php
/* @var $this CarsController */
/* @var $data Car */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('VIN')); ?>:</b>
	<?php echo CHtml::encode($data->VIN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('brand')); ?>:</b>
	<?php echo CHtml::encode($data->brand); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marka')); ?>:</b>
	<?php echo CHtml::encode($data->marka); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('specifications')); ?>:</b>
	<?php echo CHtml::encode($data->specifications); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('issue_year')); ?>:</b>
	<?php echo CHtml::encode($data->issue_year); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('engine_number')); ?>:</b>
	<?php echo CHtml::encode($data->engine_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('engine_volume')); ?>:</b>
	<?php echo CHtml::encode($data->engine_volume); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('color')); ?>:</b>
	<?php echo CHtml::encode($data->color); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anti_block')); ?>:</b>
	<?php echo CHtml::encode($data->anti_block); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anti_block_comment')); ?>:</b>
	<?php echo CHtml::encode($data->anti_block_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anti_slip')); ?>:</b>
	<?php echo CHtml::encode($data->anti_slip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anti_slip_comment')); ?>:</b>
	<?php echo CHtml::encode($data->anti_slip_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sensor')); ?>:</b>
	<?php echo CHtml::encode($data->sensor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sensor_comment')); ?>:</b>
	<?php echo CHtml::encode($data->sensor_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('conditioner')); ?>:</b>
	<?php echo CHtml::encode($data->conditioner); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('conditioner_comment')); ?>:</b>
	<?php echo CHtml::encode($data->conditioner_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('climat_control')); ?>:</b>
	<?php echo CHtml::encode($data->climat_control); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('climat_control_comment')); ?>:</b>
	<?php echo CHtml::encode($data->climat_control_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rain_sensor')); ?>:</b>
	<?php echo CHtml::encode($data->rain_sensor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rain_sensor_comment')); ?>:</b>
	<?php echo CHtml::encode($data->rain_sensor_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xenon')); ?>:</b>
	<?php echo CHtml::encode($data->xenon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xenon_comment')); ?>:</b>
	<?php echo CHtml::encode($data->xenon_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('course_stabilizee')); ?>:</b>
	<?php echo CHtml::encode($data->course_stabilizee); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('course_stabilizee_comment')); ?>:</b>
	<?php echo CHtml::encode($data->course_stabilizee_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seat_heating')); ?>:</b>
	<?php echo CHtml::encode($data->seat_heating); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seat_heating_comment')); ?>:</b>
	<?php echo CHtml::encode($data->seat_heating_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hatch')); ?>:</b>
	<?php echo CHtml::encode($data->hatch); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hatch_comment')); ?>:</b>
	<?php echo CHtml::encode($data->hatch_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PDS')); ?>:</b>
	<?php echo CHtml::encode($data->PDS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PDS_comment')); ?>:</b>
	<?php echo CHtml::encode($data->PDS_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('multimedia')); ?>:</b>
	<?php echo CHtml::encode($data->multimedia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('multimedia_comment')); ?>:</b>
	<?php echo CHtml::encode($data->multimedia_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('electro_elevator')); ?>:</b>
	<?php echo CHtml::encode($data->electro_elevator); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('electro_elevator_comment')); ?>:</b>
	<?php echo CHtml::encode($data->electro_elevator_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('electro_mirors')); ?>:</b>
	<?php echo CHtml::encode($data->electro_mirors); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('electro_mirors_comment')); ?>:</b>
	<?php echo CHtml::encode($data->electro_mirors_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mirrors_heating')); ?>:</b>
	<?php echo CHtml::encode($data->mirrors_heating); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mirrors_heating_comment')); ?>:</b>
	<?php echo CHtml::encode($data->mirrors_heating_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('additional_equipment')); ?>:</b>
	<?php echo CHtml::encode($data->additional_equipment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('out_of_equipment')); ?>:</b>
	<?php echo CHtml::encode($data->out_of_equipment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('show')); ?>:</b>
	<?php echo CHtml::encode($data->show); ?>
	<br />

	*/ ?>

</div>