<?php
/* @var $this CarsController */
/* @var $model Car */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'car-form',
	// Please Нетte: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	<?php echo $form->errorSummary($model); ?>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'user_id', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'user_id', CHtml::listData(Client::model()->findAll(), 'id', 'login'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'user_id',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'VIN', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'VIN', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'VIN',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'brand', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'brand', array('maxlength' => 150, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'brand',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'marka', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'marka', array('maxlength' => 150, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'marka',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'specifications', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'specifications', array('maxlength' => 150, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'specifications',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'issue_year', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'issue_year', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'issue_year',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'engine_number', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'engine_number', array('maxlength' => 150, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'engine_number',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'engine_volume', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'engine_volume', array('maxlength' => 150, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'engine_volume',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'color', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'color', array('maxlength' => 150, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'color',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'anti_block', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'anti_block', array(Car::FALSE=>'Нет',Car::TRUE=>'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'anti_block',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'anti_block_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'anti_block_comment', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'anti_block_comment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'anti_slip', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'anti_slip', array(Car::FALSE=>'Нет',Car::TRUE=>'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'anti_slip',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'anti_slip_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'anti_slip_comment', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'anti_slip_comment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'sensor', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'sensor', array(Car::FALSE=>'Нет',Car::TRUE=>'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'sensor',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'sensor_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'sensor_comment', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'sensor_comment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'conditioner', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'conditioner', array(Car::FALSE=>'Нет',Car::TRUE=>'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'conditioner',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'conditioner_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'conditioner_comment', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'conditioner_comment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'climat_control', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'climat_control', array(Car::FALSE=>'Нет',Car::TRUE=>'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'climat_control',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'climat_control_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'climat_control_comment', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'climat_control_comment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'rain_sensor', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'rain_sensor', array(Car::FALSE=>'Нет',Car::TRUE=>'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'rain_sensor',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'rain_sensor_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'rain_sensor_comment', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'rain_sensor_comment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'xenon', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'xenon', array(Car::FALSE=>'Нет',Car::TRUE=>'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'xenon',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'xenon_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'xenon_comment', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'xenon_comment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'course_stabilizee', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'course_stabilizee', array(Car::FALSE=>'Нет',Car::TRUE=>'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'course_stabilizee',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'course_stabilizee_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'course_stabilizee_comment', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'course_stabilizee_comment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'seat_heating', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'seat_heating', array(Car::FALSE=>'Нет',Car::TRUE=>'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'seat_heating',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'seat_heating_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'seat_heating_comment', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'seat_heating_comment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'hatch', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'hatch', array(Car::FALSE=>'Нет',Car::TRUE=>'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'hatch',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'hatch_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'hatch_comment', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'hatch_comment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'PDS', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'PDS', array(Car::FALSE=>'Нет',Car::TRUE=>'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'PDS',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'PDS_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'PDS_comment', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'PDS_comment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'multimedia', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'multimedia', array(Car::FALSE=>'Нет',Car::TRUE=>'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'multimedia',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'multimedia_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'multimedia_comment', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'multimedia_comment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'electro_elevator', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'electro_elevator', array(Car::FALSE=>'Нет',Car::TRUE=>'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'electro_elevator',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'electro_elevator_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'electro_elevator_comment', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'electro_elevator_comment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'electro_mirors', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'electro_mirors', array(Car::FALSE=>'Нет',Car::TRUE=>'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'electro_mirors',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'electro_mirors_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'electro_mirors_comment', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'electro_mirors_comment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'mirrors_heating', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'mirrors_heating', array(Car::FALSE=>'Нет',Car::TRUE=>'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'mirrors_heating',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'mirrors_heating_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'mirrors_heating_comment', array('maxlength' => 20, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'mirrors_heating_comment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'additional_equipment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'additional_equipment', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'additional_equipment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'out_of_equipment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?php echo $form->textField($model, 'out_of_equipment', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
        </div>
        <?=$form->error($model, 'out_of_equipment',array('class'=>"label label-danger col-xs-10")); ?>
    </div>
    <div class="row" style="margin-bottom: 13px;">
        <div class="input-group col-xs-10">
            <?=$form->labelEx($model,'show', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
            <?=$form->dropDownList($model, 'show', array(Car::CAR_HIDDEN => 'Нет', Car::CAR_VISIBLE => 'Да'), array('class' => 'form-control'));?>
        </div>
        <?=$form->error($model, 'show',array('show'=>"label label-danger col-xs-10")); ?>
    </div>

	<div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',array('class'=>'btn btn-success')); ?>
    </div>

<?php $this->endWidget(); ?>
</div>