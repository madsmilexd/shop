<?php
/* @var $this ClientController */
/* @var $model Client */

$this->breadcrumbs=array(
	'Clients'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Управление клиентами', 'url'=>array('admin')),
);
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Создать клиента</h1>
    </div>
    <div class="col-xs-12 block_inner">
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
        </div></div>