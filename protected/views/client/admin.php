<?php
/* @var $this ClientController */
/* @var $model Client */

$this->breadcrumbs=array(
	'Clients'=>array('index'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Создать клиента', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#client-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
<h1>Управление Клиентами</h1>
    </div>
    <div class="col-xs-12 block_inner">

<?php echo CHtml::link('Продвинутый поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'client-grid',
    'type' => 'striped bordered condensed',
	'dataProvider'=>$model->search(),
    'summaryText'=>false,
//	'filter'=>$model,
	'columns'=>array(
		'id',
		'login',
		'name',
		'surname',
		'father_name',
		'tel',
		/*
		'tel_coment',
		'tel2',
		'tel2_comment',
		'tel3',
		'tel3_comment',
		'email',
		'email2',
		'password',
		'city_id',
		'organization',
		'active',
		*/
        array(
            'header' => 'Actions',
            'class' => 'CButtonColumn',
            'template' => '{update}{view}{pass}{delete}',
            'htmlOptions' => array('class'=>'btn-group col-xs-3'),
            'buttons' => array
            (
                'update' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class="glyphicon glyphicon-pencil"></span>',
                    'options' => array('title' => 'Редактировать', 'class' => 'text-center btn btn-success '),
                    'url'=>'Yii::app()->createUrl("client/update/", array("id"=>$data->id))',
                    'click'=>'',
                ),

                'view' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class=" glyphicon glyphicon-eye-open"></span>',
                    'options' => array('title' => 'Просмотр', 'class' => 'text-center btn btn-info'),
                    'url'=>'Yii::app()->createUrl("client/view", array("id"=>$data->id))',
                    'click'=>'',
                ),
                'pass' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class=" glyphicon glyphicon-envelope"></span>',
                    'options' => array('title' => 'Востановить пароль', 'class' => 'text-center btn btn-warning'),
                    'url'=>'Yii::app()->createUrl("client/sendpassword", array("id"=>$data->id))',

                ),
                'delete' => array
                (
                    'imageUrl' => false,
                    'label' => '<span type="button" class=" glyphicon glyphicon-remove-circle"></span>',
                    'options' => array('title' => 'Удалить', 'class' => 'text-center btn btn-danger'),
                    'url'=>'Yii::app()->createUrl("client/delete", array("id"=>$data->id))',
                ),

            ),
        ),
	),
)); ?>
    </div>
</div>