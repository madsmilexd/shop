<?php
/* @var $this ClientController */
/* @var $model Client */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'client-form',
    'htmlOptions' => array('class' => 'form-horizontal','style'=>'padding-left:50px;'),
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'login', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'login', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'login',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'name', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'name', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'name',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'surname', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'surname', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'surname',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'father_name', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'father_name', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'father_name',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'tel', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'tel', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'tel',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'tel_coment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'tel_coment', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'tel_coment',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'tel2', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'tel2', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'tel2',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'tel2_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'tel2_comment', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'tel2_comment',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'tel3', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'tel3', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'tel3',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'tel3_comment', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'tel3_comment', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'tel3_comment',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'email', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'email', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'email',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'email2', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'email2', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'email2',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'password', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'password', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'password',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'city_id', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'city_id', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'city_id',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'organization', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?php echo $form->textField($model, 'organization', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
    </div>
    <?=$form->error($model, 'organization',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row" style="margin-bottom: 13px;">
    <div class="input-group col-xs-10">
        <?=$form->labelEx($model,'active', array('class'=>'input-group-addon alert-info','style'=>'width: 30%')); ?>
        <?=$form->dropDownList($model, 'active', array(Client::STATUS_ACTIVE => 'Активирован', Client::STATUS_UNACTIVE => 'Не активирован'), array('class' => 'form-control'));?>
    </div>
    <?=$form->error($model, 'active',array('class'=>"label label-danger col-xs-10")); ?>
</div>

<div class="row buttons">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',array('class'=>'btn btn-success')); ?>
</div>
<?php $this->endWidget(); ?>
