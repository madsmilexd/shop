<?php
/* @var $this ClientController */
/* @var $model Client */

$this->breadcrumbs=array(
	'Clients'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Управление клиентами', 'url'=>array('index')),
	array('label'=>'Создать клиента', 'url'=>array('create')),
	array('label'=>'Просмотр клиента', 'url'=>array('view', 'id'=>$model->id)),
);
?>
<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Редактировать клиента №<?php echo $model->id; ?></h1>
    </div>
    <div class="col-xs-12 block_inner">
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
        </div></div>