<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Проверка email</h1>
    </div>
    <div class="col-xs-12 block_inner">

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'category-form',
            'enableAjaxValidation'=>false,
        )); ?>

        <div class="row" style="margin-bottom: 13px;">
            <div class="input-group col-xs-10">
                <?=$form->labelEx($model,'email', array('class'=>'input-group-addon alert-danger','style'=>'width: 30%')); ?>
                <?php echo $form->textField($model, 'email', array('maxlength' => 255, 'class' => 'form-control','style' => 'height: 100%')); ?>
            </div>
            <?=$answer;?>
            <?=$form->error($model, 'email',array('class'=>"label label-danger col-xs-10")); ?>
        </div>

        <div class="row buttons">
            <?php echo CHtml::submitButton('Поиск',array('class'=>'btn btn-success')); ?>
        </div>

        <?php $this->endWidget(); ?>

        </div></div>