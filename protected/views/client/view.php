<?php
/* @var $this ClientController */
/* @var $model Client */

$this->breadcrumbs=array(
	'Clients'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Управление клиентами', 'url'=>array('index')),
	array('label'=>'Создать клиента', 'url'=>array('create')),
	array('label'=>'Редактировать клиента', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить клиента', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы действительно хотите удалить?')),
);
?>

<div class="col-xs-12 block">
    <div class="col-xs-12 block_header">
        <h1>Просмотр клиента №<?php echo $model->id; ?></h1>
    </div>
    <div class="col-xs-12 block_inner">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'login',
		'name',
		'surname',
		'father_name',
		'tel',
		'tel_coment',
		'tel2',
		'tel2_comment',
		'tel3',
		'tel3_comment',
		'email',
		'email2',
		'password',
		'city_id',
		'organization',
		'active',
	),
)); ?>
        </div></div>
