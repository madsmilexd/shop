<?php

/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 18.09.14
 * Time: 18:42
 */
class ShoppingCart
{
    public function getItems()
    {
        $res = array();
        $cart = Yii::app()->session['cart'];
        for ($i = 0; $i < count($cart); $i++) {
            $res[$i] = unserialize($cart[$i]);
        }
        return $res;
    }

    public function init()
    {
        //echo count(Yii::app()->session['cart']);
    }

    public function put($id)
    {
        $temp = array();
        $goods = Goods::model()->findByPk($id);
        $in_cart = false;
        for ($q = 0; $q < count(Yii::app()->session['cart']); $q++) {
            $temp[$q] = Yii::app()->session['cart'][$q];
            if (!$in_cart) {
                $item = unserialize(Yii::app()->session['cart'][$q]);
                if ($item->id == $goods->id) {
                    $in_cart = true;
                }
            }
        }

        if (!$in_cart) {
            $temp[count($temp)] = serialize($goods);
        }

        Yii::app()->session['cart'] = $temp;

    }

    public function remove($id)
    {
        $temp = array();
        $i = 0;

        foreach (Yii::app()->session['cart'] as $item) {
            $uns = unserialize($item);
            if ($uns->id != $id) {
                $temp[$i++] = $item;
            }

            Yii::app()->session['cart'] = $temp;
        }


        return true;
    }

    public function clear()
    {
        unset(Yii::app()->session['cart']);
    }

    public function makeCartList()
    {
        $res = '';
        if (Yii::app()->session['cart']) {
            foreach (Yii::app()->session['cart'] as $item) {
                $res .= self::makeUrl($item);
            }
            $res .= '<a href="' . Yii::app()->createAbsoluteUrl('site/checkout/') . '">
                <div class="cart_button_green col-xs-12">
                    Оформить заказ
                </div>
        	</a>';
            $res .= '<a href="' . Yii::app()->createAbsoluteUrl('site/clear/') . '">
                <div class="cart_button_red col-xs-12">
                Очистить корзину
                </div>
            </a>';
        } else {
            $res .= '<a href="' . Yii::app()->createAbsoluteUrl('site/catalog/') . '"class="list-group-item last-item
	list-group-item-info">Открыть
	каталог
	</a>';
        }
        return $res;
        //var_dump(Yii::app()->session['cart']);
    }

    public function makeUrl($elem)
    {
        /** @var Goods $item */
        $item = unserialize($elem);
        $res = '
		 <div class="col-xs-12 cart_item"">
             <a href="' . Yii::app()->createAbsoluteUrl('site/view/' . $item->id) .'">
                 <div class="col-xs-10 cart_item_name" title="'. $item->name . '(' . $item->getPriceLabelTitle() . ' руб.)">'.
                    $item->name . '(' . $item->getPriceLabel() . ' руб.)
                </div>
             </a>
                  <a href="' . Yii::app()->createAbsoluteUrl('site/unsete/' .$item->id) . '">
                    <div class="col-xs-2 cart_item_button">
                    <span class="glyphicon glyphicon-remove-circle"></span>
                    </div>
                  </a>
        </div>
        ';

        return $res;
    }

    public function goodsToArray($goods)
    {
        return array('id' => $goods->id, 'price' => $goods->pricex100, 'name' => $goods->name);
    }
}
