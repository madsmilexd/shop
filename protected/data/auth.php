<?php
return array (
  'editComment' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'editPost' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'editUser' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'editUserStatus' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'editOwnComment' => 
  array (
    'type' => 1,
    'description' => '',
    'bizRule' => 'return Yii::app()->user->id == $params["model"]->user_id;',
    'data' => NULL,
    'children' => 
    array (
      0 => 'editComment',
    ),
  ),
  'editOwnPost' => 
  array (
    'type' => 1,
    'description' => '',
    'bizRule' => 'return Yii::app()->user->id == $params["model"]->user_id;',
    'data' => NULL,
    'children' => 
    array (
      0 => 'editPost',
    ),
  ),
  'editOwnUser' => 
  array (
    'type' => 1,
    'description' => '',
    'bizRule' => 'return Yii::app()->user->id == $params["$model"]->user_id',
    'data' => NULL,
    'children' => 
    array (
      0 => 'editUser',
    ),
  ),
  0 => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'editOwnComment',
      1 => 'editOwnPost',
      2 => 'editOwnUser',
    ),
  ),
  1 => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 0,
      1 => 'editComment',
      2 => 'editPost',
      3 => 'editUserStatus',
    ),
    'assignments' => 
    array (
      2 => 
      array (
        'bizRule' => NULL,
        'data' => NULL,
      ),
    ),
  ),
  2 => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 1,
      1 => 'editUser',
    ),
    'assignments' => 
    array (
      1 => 
      array (
        'bizRule' => NULL,
        'data' => NULL,
      ),
    ),
  ),
);
