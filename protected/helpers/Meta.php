<?php

class Meta
{

	public static  function make($con ,$title = '', $keywords = '', $description = '')
	{
		$t = Yii::app()->params['project'];
		if($title){
			$t.= ' - '.$title;
		}


		$con->setPageTitle($t);


		if(Yii::app()->params['enable_default_meta']){
			Yii::app()->clientScript
				->registerMetaTag(($description)?$description:Yii::app()->params['default_ds'], 'description') //meta
				->registerMetaTag(($keywords)?$keywords:Yii::app()->params['default_kw'], 'keywords');
		}else{
			if($description)
			Yii::app()->clientScript->registerMetaTag($description, 'description'); //meta
			if($keywords)
			Yii::app()->clientScript->registerMetaTag($keywords, 'keywords');
		}

	}


} 