<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property string $path
 *
 * The followings are the available model relations:
 * @property Category $parent
 * @property Category[] $categories
 * @property Goods[] $goods
 */
class Category extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('parent_id', 'numerical', 'integerOnly'=>true),
			array('name, path', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, parent_id, path', 'safe', 'on'=>'search'),
		);
	}


	public function behaviors()
	{
		return array(
			'hierarchy' => array(
				'class' => 'application.behaviors.HierarchyBehavior',
				//'property1'=>'value1',
				//'property2'=>'value2',
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent' => array(self::BELONGS_TO, 'Category', 'parent_id'),
			'categories' => array(self::HAS_MANY, 'Category', 'parent_id'),
			'goods' => array(self::HAS_MANY, 'Goods', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Имя',
			'parent_id' => 'Родитель',
			'path' => 'Путь',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('path',$this->path,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getAll($array = true){

		if($array){
			$all = self::model()->findAll();
			$result = array();
			foreach($all as $a){
				$result[$a->id] = $a->name;
			}
			return $result;
		}else{
			return self::model()->findAll();
		}

	}

	public static function getArray(){
		$array = array(0 => 'Нет категории');
		$cats = self::model()->findAll();
		foreach($cats as $c){
			$array[$c->id] = $c->name;
		}
		return $array;
	}

//    public function afterSave(){
//        parent::afterSave();
//
//        if($this->isNewRecord)
//            Category::model()->rebuildPaths();
//    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
