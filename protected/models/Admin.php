<?php

/**
 * This is the model class for table "admins".
 *
 * The followings are the available columns in table 'admins':
 * @property integer $id
 * @property integer $role_id
 * @property string $login
 * @property string $pass
 * @property string $email
 * @property string $description
 */
class Admin extends CActiveRecord
{
    const AZAZAZA = 'azazaza';
	const STATUS_ADMIN = 2;
	const STATUS_MANAGER = 1;
	const STATUS_USER = 0;

	public $hashed_password;
    public $pass2;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admins';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('login, pass, email', 'required'),
			array('pass2', 'required','on'=>'ChangePass'),
			array('login, email', 'unique'),
            array('pass', 'compare', 'compareAttribute' => 'pass2', 'on' => 'ChangePass'),
			array('email','email'),
			array('role_id', 'numerical', 'integerOnly'=>true),
			array('login, pass, email', 'length', 'max'=>255),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, role_id, login, pass, email, description', 'safe', 'on'=>'search'),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'role_id' => 'Роль',
			'login' => 'Логин',
			'pass' => 'Пароль',
			'pass2' => 'Повторите пароль',
			'email' => 'Email',
			'description' => 'Описание',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('role_id',$this->role_id);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('pass',$this->pass,true);
		$criteria->compare('pass2',$this->pass2,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function makeAdmin(){
		$this->role_id = Admin::STATUS_ADMIN;
		return $this->save();
	}

	public function makeManager(){
		$this->role_id = Admin::STATUS_MANAGER;
		return $this->save();
	}

	public function makeUser(){
		$this->role_id = Admin::STATUS_USER;
		return $this->save();
	}




	public function login($isUrl = false)
	{
		/** @var Admin $model */
		$model = $this;
		$model->setScenario('login');


		if ($this->email) {
			if (!$isUrl) {
				if ($model = self::findByAttributes(array('email' => $this->email))) {
					if (!$model->validatePassword($this->pass)) {
						$this->addError('password', 'Нет такого пользователя или пароль не верный');
						return false;
					}
				} else {
					$this->addError('password', 'Нет такого пользователя или пароль не верный');
					return false;
				}
			} else {
				if ($model = self::findByAttributes(array('email' => $this->email))) {
					if (!$model->validateHashPassword($this->pass)) {
						$this->addError('password', 'Нет такого пользователя или пароль не верный');
						return false;
					}
				} else {
					$this->addError('password', 'Нет такого пользователя или пароль не верный');
					return false;
				}
			}
		}
		if (!$model->id) {
			$this->addError('email', 'Пользователь не найден');
			return false;
		}


		$identity = new UserIdentity($model);
		$identity->authenticate();
		Yii::app()->user->login($identity, 3600 * 24 * 365 * 62); // 62 года
		return true;
	}
	public function validateHashPassword($pass)
	{
		return $pass == $this->pass;
	}
	public function validatePassword($pass)
	{
		return $this->cryptPass($pass) == $this->pass;
	}
	public function beforeSave()
	{
//На случай редактирования пользователя предусмотреть, что пароль не обязателен при редактировании
		if (!parent::beforeSave()) return false;
		if (($this->scenario != 'approve' || $this->scenario != 'recovery') && $this->pass) {
			$this->pass = $this->cryptPass($this->pass);
		}
		return true;
	}


	public function cryptPass($str)
	{
		return md5($str);
	}

	public function findByMail($email)
	{
		$cr = $this->getDbCriteria();
		$cr->addColumnCondition(array(
			$this->getTableAlias() . '.email' => $email,
		));
		return $this;
	}

	public function authenticate($attribute, $params)
	{
		$this->_identity = new UserIdentity($this->username, $this->hashed_password);
		if (!$this->_identity->authenticate())
			$this->addError('password', 'Неправильное имя пользователя или пароль.');
	}

	public function aproveMe($url)
	{
		$data = explode(':', base64_decode($url));
		if (count($data) != 6) {
			return false;
		}
		if ($data[0] != 'aprove_email' && $data[1] != '1') {
			return false;
		}
		$me = $this->findByMail($data[3])->find();
		/** @var User $me */
		$me->approved = 1;
		$me->scenario = 'approve';
		return $me->save();
	}
//set
	/**
	 * @param bool $recover
	 * @param string $url
	 * @param string $oldPass
	 * @param string $newPass
	 * @param string $newPassRep
	 * @param User $user
	 */
	public function changePass($recover, $url = 0, $oldPass = 0, $newPass = 0, $newPassRep = 0, $user = 0)
	{
		if ($recover) {
			$datas = explode(':', base64_decode($url));
		}
		if (($datas[1] - time()) < (24 * 60 * 60)) {
			/** @var Admin $me */
			$this->setScenario('recovery');
			$me = $this->findByMail($datas[2])->find();
			$me->setScenario('recovery');
			if ($me->pass == $datas[0]) {
				$me->attributes = Yii::app()->request->getPost('Admin');
                $pass=$me->pass ;
				$me->save();
                $me->sendMail('Ваш новый пароль: ' . $pass, 'Ваш новый пароль: ' . $pass);
			}
		}
		return true;
	}
//get
	/**
	 * @param User $user
	 * @return string
	 */
	public function passRecovery()
	{
		$str = '<a href="' . Yii::app()->controller->createAbsoluteUrl(
				'/mailur/New?url=' . str_replace('=', '', base64_encode($this->pass . ':' . time() . ':' . $this->email . ':' . $this->role_id))) . '">Ссылка</a><br/>Ваша форма для востановления пароля';
		$alt = 'Скопируйте и вставте этоу строку в поле браузера для создания нового пароля' . Yii::app()->controller->createAbsoluteUrl(
				'/mailur/New?url=' . str_replace('=', '', base64_encode($this->pass. ':' .time() . ':' . $this->email . ':' . $this->role_id))) . '\n Ваша форма для востановления пароля';
		return self::sendMail($alt, $str);
	}
//get
	public function sendMail($alt, $text)
	{
		$message = "Message sent!";
		$mail = Yii::app()->mailer;
		$mail->AddAddress($this->email, $this->login);
		$mail->IsHTML(true); // set email format to HTML
		$mail->Subject = "From ".Yii::app()->params['project'];
		$mail->Body = $text;
		$mail->AltBody = $alt;
		if (!$mail->Send()) {
			$message = "Message could not be sent. <p>";
			$message = "Mailer Error: " . $mail->ErrorInfo;
			return false;
		}
		return true;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Admins the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
