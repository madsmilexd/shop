<?php
Yii::import('application.extensions.image.Image');

/**
 * This is the model class for table "goods".
 *
 * The followings are the available columns in table 'goods':
 * @property integer $id
 * @property string $code
 * @property integer $category_id
 * @property string $name
 * @property string $path
 * @property string $description
 * @property integer $pricex100
 * @property integer $status
 * @property integer $count
 * @property integer $warehouse_id
 * @property string $kw
 * @property string $ds
 *
 * The followings are the available model relations:
 * @property Warehouses $warehouse
 * @property Category $category
 */
class Goods extends CActiveRecord
{
	public $sw;
	public $image;
	public $remove_img;
	public $old_path;



	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'goods';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code, category_id, name, description, pricex100, status, count', 'required'),
			array('image', 'file', 'types' => 'jpg, gif, png', 'allowEmpty' => true),
			array('image, remove_img, old_path, kw, ds', 'safe'),
			array('code', 'unique', 'except' => 'upd'),
			array('category_id, pricex100, status, count, warehouse_id', 'numerical', 'integerOnly' => true),
			array('code, name, path', 'length', 'max' => 255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, code, category_id, name, path, description, pricex100, status, count, warehouse_id', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'warehouse' => array(self::BELONGS_TO, 'Warehouse', 'warehouse_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code' => 'Код товара',
			'category_id' => 'Категория',
			'name' => 'Название',
			'path' => 'Папка с галлереей',
			'description' => 'Описание',
			'pricex100' => 'Цена',
			'status' => 'Статус',
			'count' => 'Количество',
			'warehouse_id' => 'Номер склада',
			'kw'=>'Meta keywords',
			'ds'=>'Meta description'
		);
	}

	public function beforeValidate()
	{
		if (parent::beforeValidate()) {
			if (empty($this->path)) {
				$this->path = 'empty.png';
				return true;
			} else {
				return true;
			}
		}
		return false;
	}

	public function afterFind()
	{
		if (parent::afterFind()) {
			$this->old_path = $this->path;
			return true;
		}
		return false;
	}


	public function getPath()
	{
//		 return str_replace('index.php','',Yii::app()->createAbsoluteUrl('')).'images/';
		return Yii::app()->baseUrl . '/images/';
	}

	public function getImage()
	{
		$url = pathinfo($this->path);
		return '<img src="' . $this->getPath() . $url['filename'] . '.' . $url['extension'] . '"/>';
	}

	public function getPreviewPath($path)
	{
		$imagePath = pathinfo($path, PATHINFO_DIRNAME);
		$imageName = pathinfo($path, PATHINFO_BASENAME);
		$imagePreviewPath = $imagePath . '/thumbs/' . $imageName;
		return $imagePreviewPath;
	}

	public function getPrevImage()
	{
		$url = pathinfo($this->path);
		return '<img width="75%" src="' . $this->getPath() . '/' . $url['filename'] . '.' . $url['extension'] . '"/>';
	}


	public function getStatus()
	{
		if ($this->status == 0) {
			return '<label class="label label-danger">Нет в наличии</label>';
		}

		if ($this->status == 1) {
			return '<label class="label label-success">Есть на складе (' . $this->count . ' шт.)</label>';
		}

		if ($this->status == 2) {
			return '<label class="label label-warning">Под заказ</label>';
		}
	}

	public function getPriceLabel()
	{
//        $price = $this->pricex100;
//        $res = '';
//        for($i = strlen($price.'')-1; $i>=0;$i--){
//
//            $res .= $price[$i];
//            if($i%3==0){
//                $res .=' ';
//            }
//        }
//		return strrev($res);
			return number_format($this->pricex100,0,' ',' ');
	}

    public function getPriceLabelTitle()
    {
//        $price = $this->pricex100;
//        $res = '';
//        for($i = strlen($price.'')-1; $i>=0;$i--){
//
//            $res .= $price[$i];
//            if($i%3==0){
//                $res .=' ';
//            }
//        }
		return number_format($this->pricex100,0,' ',' ');
	}



	public function getPriceText($prices = '')
	{
//		if ($prices == '') {
//			$item = $this;
//			if (strlen($item->pricex100) <= 2) {
//				return "00.{$item->pricex100}";
//			}
//
//			$price = '';
//			if (strlen($item->pricex100) > 2) {
//				for ($i = 0; $i < strlen($item->pricex100) - 2; $i++) {
//					$price .= $item->pricex100[$i];
//				}
//				$price .= ".{$item->pricex100[strlen($item->pricex100) - 2]}{$item->pricex100[strlen($item->pricex100) - 1]}";
//			}
//		}else{
//
//			if (strlen($prices) <= 2) {
//				return "00.{$prices}";
//			}
//
//			$price = '';
//			if (strlen($prices) > 2) {
//				for ($i = 0; $i < strlen($prices) - 2; $i++) {
//					$price .= $prices[$i];
//				}
//				$price .= ".{$prices[strlen($prices) - 2]}{$prices[strlen($prices) - 1]}";
//			}
//		}
		return $prices;
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('code', $this->code, true);
		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('path', $this->path, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('pricex100', $this->pricex100);
		$criteria->compare('status', $this->status);
		$criteria->compare('count', $this->count);
		$criteria->compare('warehouse_id', $this->warehouse_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function byCategory($id)
	{

		/** @var Category $cat */
		$cat = Category::model()->findByPk($id);


		$criteria=new CDbCriteria;
		$criteria->condition='path LIKE :path';
		$criteria->params=array(':path'=>$cat->path.'%');
		$categories = Category::model()->findAll($criteria);

		$c = array();
		/** @var Category $c */
		foreach($categories as $s){
			array_push($c,$s->id);
		}

		$cr = $this->getDbCriteria();
		$cr->addInCondition('category_id',$c);
		return $this;
	}



	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Goods the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
