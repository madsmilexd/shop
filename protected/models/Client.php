<?php

/**
 * This is the model class for table "clients".
 *
 * The followings are the available columns in table 'clients':
 * @property integer $id
 * @property string $login
 * @property string $name
 * @property string $surname
 * @property string $father_name
 * @property string $tel
 * @property string $tel_coment
 * @property string $tel2
 * @property string $tel2_comment
 * @property string $tel3
 * @property string $tel3_comment
 * @property string $email
 * @property string $email2
 * @property string $password
 * @property integer $city_id
 * @property string $organization
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Cars[] $cars
 * @property Cities $city
 */
class Client extends CActiveRecord
{
	const STATUS_ACTIVE = 1;
	const STATUS_UNACTIVE = 0;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clients';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('login, name', 'required'),
            array('email,email2', 'unique'),
			array('city_id, active', 'numerical', 'integerOnly'=>true),
			array('login, name, surname, father_name, tel, tel_coment, tel2, tel2_comment, tel3, tel3_comment,
			email, email2, password, organization', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, login, name, surname, father_name, tel, tel_coment, tel2, tel2_comment, tel3, tel3_comment,
			email, email2, password, city_id, organization, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cars' => array(self::HAS_MANY, 'Cars', 'user_id'),
			'city' => array(self::BELONGS_TO, 'Cities', 'city_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login' => 'Логин',
			'name' => 'Имя',
			'surname' => 'Фамилия',
			'father_name' => 'Отчество',
			'tel' => 'Телефон',
			'tel_coment' => 'Комментарий к телефону',
			'tel2' => 'Рабочий телефон',
			'tel2_comment' => 'Комментарий к рабочему телефону',
			'tel3' => 'Мобильный',
			'tel3_comment' => 'Комментарий к мобильному',
			'email' => 'Email',
			'email2' => 'Второй email',
			'password' => 'Пароль',
			'city_id' => 'Город',
			'organization' => 'Организация',
			'active' => 'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('father_name',$this->father_name,true);
		$criteria->compare('tel',$this->tel,true);
		$criteria->compare('tel_coment',$this->tel_coment,true);
		$criteria->compare('tel2',$this->tel2,true);
		$criteria->compare('tel2_comment',$this->tel2_comment,true);
		$criteria->compare('tel3',$this->tel3,true);
		$criteria->compare('tel3_comment',$this->tel3_comment,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('email2',$this->email2,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('organization',$this->organization,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function delete(){
		$this->active = self::STATUS_UNACTIVE;
		return $this->save();
	}

	public function activate(){
		$this->active = self::STATUS_ACTIVE;
		return $this->save();
	}

    public function sendMail($text,$email)
    {
        $message = "Message sent!";
        $mail = Yii::app()->mailer;
        $mail->AddAddress($email);
        $mail->IsHTML(true); // set email format to HTML
        $mail->Subject = "From some good man";
        $mail->Body = $text;
        if (!$mail->Send()) {
            $message = "Message could not be sent. <p>";
            $message = "Mailer Error: " . $mail->ErrorInfo;
            return false;
        }
        return true;
    }


    /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Client the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
