<?php

/**
 * This is the model class for table "cars".
 *
 * The followings are the available columns in table 'cars':
 * @property integer $id
 * @property integer $user_id
 * @property string $VIN
 * @property string $brand
 * @property string $marka
 * @property string $specifications
 * @property integer $issue_year
 * @property string $engine_number
 * @property string $engine_volume
 * @property string $color
 * @property string $anti_block
 * @property string $anti_block_comment
 * @property string $anti_slip
 * @property string $anti_slip_comment
 * @property string $sensor
 * @property string $sensor_comment
 * @property string $conditioner
 * @property string $conditioner_comment
 * @property string $climat_control
 * @property string $climat_control_comment
 * @property string $rain_sensor
 * @property string $rain_sensor_comment
 * @property string $xenon
 * @property string $xenon_comment
 * @property string $course_stabilizee
 * @property string $course_stabilizee_comment
 * @property string $seat_heating
 * @property string $seat_heating_comment
 * @property string $hatch
 * @property string $hatch_comment
 * @property string $PDS
 * @property string $PDS_comment
 * @property string $multimedia
 * @property string $multimedia_comment
 * @property string $electro_elevator
 * @property string $electro_elevator_comment
 * @property string $electro_mirors
 * @property string $electro_mirors_comment
 * @property string $mirrors_heating
 * @property string $mirrors_heating_comment
 * @property string $additional_equipment
 * @property string $out_of_equipment
 * @property integer $show
 *
 * The followings are the available model relations:
 * @property Clients $user
 */
class Car extends CActiveRecord
{


	const CAR_HIDDEN = 0;
	const CAR_VISIBLE = 1;
    const FALSE = 0;
    const TRUE = 1;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cars';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, brand, marka', 'required'),
			array('VIN, specifications, issue_year, engine_number, engine_volume, color,
			anti_block, anti_block_comment, anti_slip, anti_slip_comment, sensor, sensor_comment, conditioner,
			conditioner_comment, climat_control, climat_control_comment, rain_sensor, rain_sensor_comment, xenon,
			xenon_comment, course_stabilizee, course_stabilizee_comment, seat_heating, seat_heating_comment, hatch,
			hatch_comment, PDS, PDS_comment, multimedia, multimedia_comment, electro_elevator,
			electro_elevator_comment, electro_mirors, electro_mirors_comment, mirrors_heating,
			mirrors_heating_comment, additional_equipment, out_of_equipment, show', 'safe'),
			array('user_id, issue_year, show', 'numerical', 'integerOnly'=>true),
			array('issue_year','length','min'=>1),
			array('issue_year','length','max'=>4),
			array('VIN, anti_block_comment, anti_slip_comment, sensor_comment, conditioner_comment,
			climat_control_comment, rain_sensor_comment, xenon_comment, course_stabilizee_comment,
			seat_heating_comment, hatch_comment, PDS_comment, multimedia_comment, electro_elevator_comment,
			electro_mirors_comment, mirrors_heating_comment', 'length', 'max'=>20),
			array('brand, marka, specifications, engine_number, engine_volume, color', 'length', 'max'=>150),
			array('anti_block, anti_slip, sensor, conditioner, climat_control, rain_sensor, xenon,
			course_stabilizee, seat_heating, hatch, PDS, multimedia, electro_elevator, electro_mirors,
			mirrors_heating', 'length', 'max'=>1),
			array('additional_equipment, out_of_equipment', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, VIN, brand, marka, specifications, issue_year, engine_number, engine_volume, color,
			anti_block, anti_block_comment, anti_slip, anti_slip_comment, sensor, sensor_comment, conditioner,
			conditioner_comment, climat_control, climat_control_comment, rain_sensor, rain_sensor_comment, xenon,
			xenon_comment, course_stabilizee, course_stabilizee_comment, seat_heating, seat_heating_comment, hatch,
			hatch_comment, PDS, PDS_comment, multimedia, multimedia_comment, electro_elevator,
			electro_elevator_comment, electro_mirors, electro_mirors_comment, mirrors_heating,
			mirrors_heating_comment, additional_equipment, out_of_equipment, show', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Clients', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'Пользователь',
			'VIN' => 'VIN',
			'brand' => 'Бренд',
			'marka' => 'Марка',
			'specifications' => 'Характеристики',
			'issue_year' => 'Год выпуска',
			'engine_number' => 'Номер двигателя',
			'engine_volume' => 'Объем двигателя',
			'color' => 'Цвет',
			'anti_block' => 'Антиблокировка',
			'anti_block_comment' => 'Комментарий к антиблокировке',
			'anti_slip' => 'Система против скольжения',
			'anti_slip_comment' => 'Комментарий к системе против скольжения',
			'sensor' => 'Сенсоры',
			'sensor_comment' => 'Комментарий к сенсорам',
            'conditioner' => 'Кондиционер',
			'conditioner_comment' => 'Комментарий к кондиционеру',
			'climat_control' => 'Климат контроль',
			'climat_control_comment' => 'Комментарий к климат контролю',
			'rain_sensor' => 'Датчик дождя',
			'rain_sensor_comment' => 'Комментарий к датчику дождя',
			'xenon' => 'Ксенон',
			'xenon_comment' => 'Комментарий к ксенону',
			'course_stabilizee' => 'Стабилизация направления',
			'course_stabilizee_comment' => 'Комментарий к стабилизации направления',
			'seat_heating' => 'Подогрев сидений',
			'seat_heating_comment' => 'Комментарий к подогреву сидений',
			'hatch' => 'Люк',
			'hatch_comment' => 'Комментарий к люку',
			'PDS' => 'Pds',
			'PDS_comment' => 'Комментарии к Psd',
			'multimedia' => 'Мультимедиа',
			'multimedia_comment' => 'Комментарий к мультимедии',
			'electro_elevator' => 'Стеклоподъемники',
			'electro_elevator_comment' => 'Комментарий к стеклоподъемникам',
			'electro_mirors' => 'Автоматические зеркала',
			'electro_mirors_comment' => 'Комментарий к автоматическим зеркалам',
			'mirrors_heating' => 'Подогрев стекол',
			'mirrors_heating_comment' => 'Комментарий к подогреву стекол',
			'additional_equipment' => 'Дополнительное оборудование',
			'out_of_equipment' => 'Отсуцтвующее оборудование',
			'show' => 'Выводить ли на страницу?',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('VIN',$this->VIN,true);
		$criteria->compare('brand',$this->brand,true);
		$criteria->compare('marka',$this->marka,true);
		$criteria->compare('specifications',$this->specifications,true);
		$criteria->compare('issue_year',$this->issue_year);
		$criteria->compare('engine_number',$this->engine_number,true);
		$criteria->compare('engine_volume',$this->engine_volume,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('anti_block',$this->anti_block,true);
		$criteria->compare('anti_block_comment',$this->anti_block_comment,true);
		$criteria->compare('anti_slip',$this->anti_slip,true);
		$criteria->compare('anti_slip_comment',$this->anti_slip_comment,true);
		$criteria->compare('sensor',$this->sensor,true);
		$criteria->compare('sensor_comment',$this->sensor_comment,true);
		$criteria->compare('conditioner',$this->conditioner,true);
		$criteria->compare('conditioner_comment',$this->conditioner_comment,true);
		$criteria->compare('climat_control',$this->climat_control,true);
		$criteria->compare('climat_control_comment',$this->climat_control_comment,true);
		$criteria->compare('rain_sensor',$this->rain_sensor,true);
		$criteria->compare('rain_sensor_comment',$this->rain_sensor_comment,true);
		$criteria->compare('xenon',$this->xenon,true);
		$criteria->compare('xenon_comment',$this->xenon_comment,true);
		$criteria->compare('course_stabilizee',$this->course_stabilizee,true);
		$criteria->compare('course_stabilizee_comment',$this->course_stabilizee_comment,true);
		$criteria->compare('seat_heating',$this->seat_heating,true);
		$criteria->compare('seat_heating_comment',$this->seat_heating_comment,true);
		$criteria->compare('hatch',$this->hatch,true);
		$criteria->compare('hatch_comment',$this->hatch_comment,true);
		$criteria->compare('PDS',$this->PDS,true);
		$criteria->compare('PDS_comment',$this->PDS_comment,true);
		$criteria->compare('multimedia',$this->multimedia,true);
		$criteria->compare('multimedia_comment',$this->multimedia_comment,true);
		$criteria->compare('electro_elevator',$this->electro_elevator,true);
		$criteria->compare('electro_elevator_comment',$this->electro_elevator_comment,true);
		$criteria->compare('electro_mirors',$this->electro_mirors,true);
		$criteria->compare('electro_mirors_comment',$this->electro_mirors_comment,true);
		$criteria->compare('mirrors_heating',$this->mirrors_heating,true);
		$criteria->compare('mirrors_heating_comment',$this->mirrors_heating_comment,true);
		$criteria->compare('additional_equipment',$this->additional_equipment,true);
		$criteria->compare('out_of_equipment',$this->out_of_equipment,true);
		$criteria->compare('show',$this->show);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function delete(){
		return $this->setHide();
	}

	public function setHide(){
		$this->show = self::CAR_HIDDEN;
		return $this->save();
	}

	public function setUnhide(){
		$this->show = self::CAR_VISIBLE;
		return $this->save();
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Car the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
