<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $id
 * @property string $client_name
 * @property string $client_email
 * @property string $client_phone
 * @property string $goods
 * @property integer $delivery
 * @property integer $sum_total
 * @property string $payment
 * @property string $address
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class Orders extends CActiveRecord
{


	const STATUS_NEW = 0;
	const STATUS_DELIVERY = 1;
	const STATUS_READY = 2;
	const STATUS_CANCELED = 3;


	const DELIVERY_TRUE = 1;
	const DELIVERY_FALSE = 0;

	private $items;
	private $oldStatus;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	public function getStatus()
	{
		return array(
			self::STATUS_NEW => 'Новый заказ',
			self::STATUS_DELIVERY => "Заказ доставляется",
			self::STATUS_READY => "Заказ завершен",
			self::STATUS_CANCELED => "Заказ отменен");
	}

	public function getDelivery()
	{
		return array(
			self::DELIVERY_FALSE => "Самовывоз",
			self::DELIVERY_TRUE => "Доставка"
		);
	}


	public function getStatusLabel($status)
	{
		$res = '';
		switch($status){
			case self::STATUS_NEW: $res = 'Новый'; break;
			case self::STATUS_DELIVERY: $res = 'Доставляентся'; break;
			case self::STATUS_CANCELED: $res = 'Отменен'; break;
			case self::STATUS_READY: $res = 'Готов'; break;
		}
		return $res;
	}

	public function getDeliveryLabel($status)
	{
		$res = '';
		switch($status){
			case self::DELIVERY_TRUE: $res="Доставка";break;
			case self::DELIVERY_FALSE: $res="самовывоз";break;
		}
		return $res;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_name, client_email, client_phone, goods, payment', 'required'),
			array('delivery, status,sum_total', 'numerical', 'integerOnly' => true),
			array('client_name, client_email, client_phone, payment, address', 'length', 'max' => 255),
			array('created_at, updated_at,sum_total', 'safe'),
			array('created_at, updated_at', 'default', 'setOnEmpty' => true, 'value' => null),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id,sum_total, client_name, client_email, client_phone, goods, delivery, payment, address, status,
			created_at, updated_at', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_name' => 'Имя клиента',
			'client_email' => 'Email клиента',
			'client_phone' => 'Телефон клиента',
			'goods' => 'Список заказов',
			'delivery' => 'Доставка',
			'payment' => 'Оплата',
			'address' => 'Адресс',
			'status' => 'Статус',
			'sum_total' => 'Общая сумма',
			'created_at' => 'Дата создания',
			'updated_at' => 'Дата редактирования',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('client_name', $this->client_name, true);
		$criteria->compare('client_email', $this->client_email, true);
		$criteria->compare('client_phone', $this->client_phone, true);
		$criteria->compare('goods', $this->goods, true);
		$criteria->compare('delivery', $this->delivery);
		$criteria->compare('payment', $this->payment, true);
		$criteria->compare('address', $this->address, true);
		$criteria->compare('status', $this->status);
		$criteria->compare('created_at', $this->created_at, true);
		$criteria->compare('updated_at', $this->updated_at, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orders the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	public function afterFind()
	{
		parent::afterFind();
		$this->oldStatus = $this->status;
	}

	public function afterSave()
	{
		parent::afterSave();
		if ($this->isNewRecord) {
			$goods = $this->parseOrder($this->goods);
			foreach ($goods as $item) {
				/** @var Goods $i */
				$i = Goods::model()->findByPk($item->id);
				$i->count = $i->count - $item->count;
				$i->save();
			}
		} else {
			if (($this->oldStatus != $this->status) && $this->status == self::STATUS_CANCELED) {

				$goods = $this->parseOrder($this->goods);
				foreach ($goods as $item) {
					/** @var Goods $i */
					$i = Goods::model()->findByPk($item->id);
					$i->count = $i->count + $item->count;
					$i->save();
				}
			}
		}
		$this->sendOrder();
	}

	public function sendOrder()
	{
		$mail = Yii::app()->mailer;
		$mail->AddAddress($this->client_email, $this->client_name);
		$mail->SetFrom(Yii::app()->params['shopEmail'], 'Отдел заказов ' . Yii::app()->params['project']);
		$mail->IsHTML(true);
		$text = '<h1>' . Yii::app()->params['project'] . '</h1>';
		$text .= '<p>Здравствуйте, ' . $this->client_name . '.</p>';
		$text .= '<p>Ваш заказ принят, наш менеджер свяжется с вами в ближайшее время</p>';
		$text .= '<p>Номер заказа ' . $this->id . '</p>';
		$text .= '<p>Вы заказали:</p>';
		$text .= '<table border="1px" style="width:600px;">';
		$text .= '<tr><td>#</td><td>Название</td><td>Цена</td><td>Количество</td><td>Итого</td></tr>';
		$items = $this->parseOrder($this->goods);
		foreach ($items as $item) {
			$ele = Goods::model()->findByPk($item->id);
			$id = $item->id;
			$name = $ele->name;
			$price = $ele->getPriceText($item->pricex100);
			$sum = ($item->pricex100) * $item->count;
			$co = $item->count;
			$text .= "<tr><td>" . $id . "</td><td>" . $name . "</td><td>" . $price . "</td><td>" . $co . "</td><td>" . $sum . "</td></tr>";
		}
		$total = ($this->sum_total);
		$checkUrl = Yii::app()->createAbsoluteUrl('orders/check/' . md5($this->client_email . '' . $this->id));
		$pj = Yii::app()->params['project'];
		$ep = Yii::app()->params['shopEmail'];
		$mt = Yii::app()->params['shopPhone'];

		$text .= '<tr><td>Всего</td><td colspan=4>' . $total . '</td></tr>';
		$text .= '</table>';
		$text .= '<p>Статус заказа можно проверить <a href="' . $checkUrl . '">тут</a> или вставьте ссылку в адресную строку
 ' . $checkUrl . ' , ';
		$text .= 'введите номер заказа в соответствующее поле, а так же ваш адрес электронной почты</p>';
		$text .= '<p>' . $pj . '</p>';
		$text .= '<p>' . $ep . '</p>';
		$text .= '<p>' . $mt . '</p>';


		$alt = '';
		$alt .= 'ShopName\n';
		$alt .= 'Здравствуйте, ' . $this->client_name . '.\n';
		$alt .= 'Ваш заказ принят, наш менеджер свяжется с вами в ближайшее время\n';
		$alt .= 'Номер заказа ' . $this->id . '\n';
		$alt .= 'Вы заказали:\n';
		$items = $this->parseOrder($this->goods);
		foreach ($items as $item) {
			$ele = Goods::model()->findByPk($item->id);
			$id = $item->id;
			$name = $ele->name;
			$price = $ele->pricex100;
			$sum = ($item->pricex100) * $item->count;
			$co = $item->count;
			$alt .= '#' . $id . ' ' . $name . ' ' . $price . 'x' . $co . '=' . $sum . '\n';
		}
		$total = ($this->sum_total);
		$checkUrl = Yii::app()->createAbsoluteUrl('orders/check/' . md5($this->client_email . '' . $this->id));
		$pj = Yii::app()->params['project'];
		$ep = Yii::app()->params['shopEmail'];
		$mt = Yii::app()->params['shopPhone'];

		$alt .= 'Всего:' . $total . '\n';

		$alt .= 'Статус заказа можно проверить вставив ссылку ' . $checkUrl . ' в адресную строку,';
		$alt .= ' введите номер заказа в соответствующее поле, а так же ваш адрес электронной почты\n';
		$alt .= $pj . '\n';
		$alt .= $ep . '\n';
		$alt .= $mt;

		$mail->Subject = "From my site";
		$mail->Body = $text;
		$mail->AltBody = $alt;
		if (!$mail->Send()) {
			return false;
		}
		return true;
	}


	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'created_at',
				'updateAttribute' => 'updated_at',
			)
		);
	}

	public function parseOrder($JSON = '')
	{
		if ($JSON == '') {
			$JSON = $this->goods;
		}
		//die($JSON);
		$res = json_decode($JSON);
		return $res;
	}
}
