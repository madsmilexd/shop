-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 29 2014 г., 13:12
-- Версия сервера: 5.5.38-log
-- Версия PHP: 5.5.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `week3`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT '0',
  `login` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `admins`
--

INSERT INTO `admins` (`id`, `role_id`, `login`, `pass`, `email`, `description`) VALUES
(1, 2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'madsmilexd@gmail.com', 'ADMIN'),
(2, 1, 'moder', '21232f297a57a5a743894a0e4a801fc3', 'moderator@gmail.com', 'moderator');

-- --------------------------------------------------------

--
-- Структура таблицы `cars`
--

CREATE TABLE IF NOT EXISTS `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `VIN` varchar(20) DEFAULT NULL,
  `brand` varchar(150) NOT NULL,
  `marka` varchar(150) NOT NULL,
  `specifications` varchar(150) DEFAULT NULL,
  `issue_year` int(11) DEFAULT NULL,
  `engine_number` varchar(150) DEFAULT NULL,
  `engine_volume` varchar(150) DEFAULT NULL,
  `color` varchar(150) DEFAULT NULL,
  `anti_block` varchar(1) DEFAULT NULL,
  `anti_block_comment` varchar(20) DEFAULT NULL,
  `anti_slip` varchar(1) DEFAULT NULL,
  `anti_slip_comment` varchar(20) DEFAULT NULL,
  `sensor` varchar(1) DEFAULT NULL,
  `sensor_comment` varchar(20) DEFAULT NULL,
  `conditioner` varchar(1) DEFAULT NULL,
  `conditioner_comment` varchar(20) DEFAULT NULL,
  `climat_control` varchar(1) DEFAULT NULL,
  `climat_control_comment` varchar(20) DEFAULT NULL,
  `rain_sensor` varchar(1) DEFAULT NULL,
  `rain_sensor_comment` varchar(20) DEFAULT NULL,
  `xenon` varchar(1) DEFAULT NULL,
  `xenon_comment` varchar(20) DEFAULT NULL,
  `course_stabilizee` varchar(1) DEFAULT NULL,
  `course_stabilizee_comment` varchar(20) DEFAULT NULL,
  `seat_heating` varchar(1) DEFAULT NULL,
  `seat_heating_comment` varchar(20) DEFAULT NULL,
  `hatch` varchar(1) DEFAULT NULL,
  `hatch_comment` varchar(20) DEFAULT NULL,
  `PDS` varchar(1) DEFAULT NULL,
  `PDS_comment` varchar(20) DEFAULT NULL,
  `multimedia` varchar(1) DEFAULT NULL,
  `multimedia_comment` varchar(20) DEFAULT NULL,
  `electro_elevator` varchar(1) DEFAULT NULL,
  `electro_elevator_comment` varchar(20) DEFAULT NULL,
  `electro_mirors` varchar(1) DEFAULT NULL,
  `electro_mirors_comment` varchar(20) DEFAULT NULL,
  `mirrors_heating` varchar(1) DEFAULT NULL,
  `mirrors_heating_comment` varchar(20) DEFAULT NULL,
  `additional_equipment` varchar(255) DEFAULT NULL,
  `out_of_equipment` varchar(255) DEFAULT NULL,
  `show` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name`, `parent_id`, `path`) VALUES
(1, 'engine', NULL, '001'),
(2, 'свеча', 1, '001.001'),
(3, '23', 1, '001.002'),
(4, 'test', 3, '001.002.001'),
(5, 'test2', 3, '001.002.002'),
(6, 'свеча24', 2, '001.001.002');

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `cities`
--

INSERT INTO `cities` (`id`, `name`) VALUES
(2, 'Kyiv'),
(3, 'Minsk');

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `tel_coment` varchar(255) DEFAULT NULL,
  `tel2` varchar(255) DEFAULT NULL,
  `tel2_comment` varchar(255) DEFAULT NULL,
  `tel3` varchar(255) DEFAULT NULL,
  `tel3_comment` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email2` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `city_id` int(255) DEFAULT NULL,
  `organization` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `city_id` (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `goods`
--

CREATE TABLE IF NOT EXISTS `goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `pricex100` int(255) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-нету,1-есть,2-под заказ',
  `count` int(11) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `ds` varchar(255) DEFAULT NULL,
  `kw` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `goods`
--

INSERT INTO `goods` (`id`, `code`, `category_id`, `name`, `path`, `description`, `pricex100`, `status`, `count`, `warehouse_id`, `ds`, `kw`) VALUES
(1, 'vm25', 1, 'свечи', 'empty.png', '<ol>\r\n	<li>Первый</li>\r\n	<li>второй</li>\r\n	<li>третий<br />\r\n	&nbsp;</li>\r\n</ol>\r\n', 18987654, 1, 144, NULL, NULL, NULL),
(2, 'vm26', 2, 'свечи', 'empty.png', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 600, 1, 90, NULL, NULL, NULL),
(3, 'vm27', 6, 'свечи', 'empty.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 12, 2, 132, NULL, NULL, NULL),
(4, 'vm26', 1, 'свечи', 'empty.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1234, 1, 145, NULL, NULL, NULL),
(5, 'mark_1', 3, 'Цилиндр', 'empty.png', '<p>Цилиндр обыкновенный</p>', 10095, 1, 5008, 1, 'марк, тест,два', 'что-то там еще'),
(6, 'mark_2', 5, 'Цилиндр', 'empty.png', '<p>Улучшеная версия марк 1</p>', 980000, 2, 3234, NULL, NULL, NULL),
(7, 'just23', 4, 'goods n1', 'empty.png', 'nop', 300, 2, 0, 1, 'testtest', 'Keyword test');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) NOT NULL,
  `client_email` varchar(255) NOT NULL,
  `client_phone` varchar(255) NOT NULL,
  `goods` text NOT NULL,
  `sum_total` int(11) DEFAULT NULL,
  `delivery` int(2) NOT NULL DEFAULT '0',
  `payment` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `client_name`, `client_email`, `client_phone`, `goods`, `sum_total`, `delivery`, `payment`, `address`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Alex', 'msxd@fatha', '380668592330', '[{"id":1,"count":10,"pricex100":18987654},{"id":5,"count":13,"pricex100":10095},{"id":6,"count":3,"pricex100":980000},{"id":2,"count":6,"pricex100":600},{"id":4,"count":21,"pricex100":1234}]', 192977289, 0, 'наличные', '', 0, NULL, NULL),
(2, 'adsas', 'asd@asd', '124124125', '[{"id":3,"count":1,"pricex100":12}]', 12, 0, 'qwe', '', 0, NULL, NULL),
(3, 'adsas', 'asd@asd', '124124125', '[{"id":3,"count":1,"pricex100":12}]', 12, 0, 'qwe', '', 0, NULL, NULL),
(4, 'adsdas', 'dasfa@asd.dw', '153678', '[{"id":3,"count":4,"pricex100":12}]', 48, 0, 'sad', '', 0, NULL, NULL),
(5, 'adsdas', 'dasfa@asd.dw', '153678', '[{"id":3,"count":4,"pricex100":12}]', 48, 0, 'sad', '', 0, NULL, NULL),
(6, 'adsdas', 'dasfa@asd.dw', '153678', '[{"id":3,"count":4,"pricex100":12}]', 48, 0, 'sad', '', 0, NULL, NULL),
(7, 'adsdas', 'dasfa@asd.dw', '153678', '[{"id":3,"count":4,"pricex100":12}]', 0, 1, 'sad', '', 1, NULL, '2014-09-26 16:46:39'),
(8, 'Alex', 'madsmilexd@gmail.com', '13245678', '[{"id":3,"count":10,"pricex100":12}]', 120, 1, 'nepru', 'skladik', 0, '2014-09-28 14:46:45', NULL),
(9, 'qewrtyy', 'adsfgh@sd.sa', '1324', '[{"id":3,"count":1,"pricex100":12}]', 12, 0, 'weafg', '', 0, '2014-09-28 14:47:59', NULL),
(10, 'Alex', 'madsmilexd@gmail.com', '380668592330', '[{"id":2,"count":1,"pricex100":600}]', 600, 0, 'nepru', '', 0, '2014-09-28 14:48:48', NULL),
(11, 'adsas', 'msxd@fatha.sa', '380668592330', '[{"id":3,"count":1,"pricex100":12}]', 12, 0, 'nepru', '', 0, '2014-09-28 14:49:19', NULL),
(12, 'adsas', 'bepscfwzopzj@dropmail.me', '380668592330', '[{"id":1,"count":1,"pricex100":18987654}]', 18987654, 0, 'nepru', '', 0, '2014-09-28 14:50:18', NULL),
(13, 'lokijhn', 'ijhn@sdf.sa', '098209876', '[{"id":3,"count":1,"pricex100":12}]', 12, 0, '098ds', '', 0, '2014-09-28 14:51:00', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `warehouses`
--

CREATE TABLE IF NOT EXISTS `warehouses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `warehouses`
--

INSERT INTO `warehouses` (`id`, `number`, `description`) VALUES
(1, 1, 'Kyiv');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `cars`
--
ALTER TABLE `cars`
  ADD CONSTRAINT `cars_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
